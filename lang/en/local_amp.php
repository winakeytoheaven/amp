<?php

/**
 * Web-service template plugin-related strings.
 *
 * NOTE: Changes here will not be seen in site activity until all Moodle caches are purged.
 *
 * @package   localamp
 * @copyright 2016 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'OHF Application Management';
$string['manage_cohorts'] = 'Manage cohorts';

// Used in introduction.php
$string['duprequesterrormsg'] = 'This request duplicates a previous request. Please contact @@OrgName@@.';

// Used in lib.php for Navigation menus
$string['amp'] = 'Application';
$string['appapply'] = 'Apply';

// Used in class.EVRManager.php
$string['evffnamelabel'] = 'First name';
$string['evflnamelabel'] = 'Last (family) name';
$string['evfphonelabel'] = 'Telephone';
$string['evfcountrylabel'] = 'Country';
$string['evfemaillabel'] = 'Email address';

$string['evfcountry'] = 'Country';

$string['evfcountry_help'] = 'Please select the country where you reside long-term, and where you will be living to begin your work if your application is accepted.';
$string['evfphone_help'] = 'Be SURE to include the country code in your phone number like this "+cc number"';

$string['evfsubmit'] = 'Submit';

$string['evfblankerror'] = 'The "{$a->label}" field cannot be empty';
$string['evfemailerror'] = 'This email address is empty or not properly structured';

// Used in evrmanagement.php
$string['confirmdelprompt'] = 'Are you SURE you want to delete the validation request from {$a->name}?';
$string['resendacknowledgement'] = 'Validation request email sent to {$a->name}';
$string['tooltipdelete'] = 'Delete the validation request from {$a->name}';
$string['tooltipdeleteblock'] = '{$a->name} has submitted an application. Delete the application and this EVR will disappear as well';
$string['tooltipedit'] = 'Edit the validation request from {$a->name}';
$string['tooltipresend'] = 'Resend a validation request to {$a->name}';

// Used in introduction.php
$string['introtitle'] = 'Email Validation';
$string['manageevrs'] = 'Manage EVRs';

// Settings default values
$string['amp_pkgselection_default'] = '';
$string['amp_validateemail_default'] = '0';
$string['amp_validationemailaddr_default'] = '';
$string['amp_validationemailbccaddr_default'] = '';
$string['amp_validationemailsubj_default'] = 'Your application to @@OrgName@@';
$string['amp_validationmsgtemplate_default'] = 'Hello @@ApplicantName@@,

Thank you for your interest in applying as a @@ApplicationTitle@@ in the @@PackageTitle@@ offered by @@OrgName@@. Use the link below to continue your application process:

@@ApplicationURL@@

Click the link (if that works) or copy the link and paste it into the address bar of your internet browser.

This will validate your email address and direct you to the on-line @@ApplicationTitle@@ application page.';
$string['amp_validationresponsepage_default'] = "<h1>THANK YOU</h1>
<p>Thank you for submitting your email address validation request. A validation email has been sent and you should receive it soon.</p>
<h1>PROBLEMS RECEIVING EMAIL MESSAGES</h1>
<p>A number of issues can prevent receipt of the validation email from this website:</p>
<ul>
<li>Sometimes it can take as much as an hour for an email message to make it through to your mailbox. Be patient, but if you don't get it within an hour then there is a problem.</li>
<li>Spam filtering at your email provider may block it. Contact your email provider for help.</li>
<li>Spam filtering at your machine's email client may block it. Adjust the filter settings on your email client.</li>
<li>The email address you entered was mistyped. Send a message to a friend and have him/her give you the address from which you sent it.</li>
<li>Your email box can be full or disabled for some other reason. Contact your email provider for help.</li>
<li>If you need help with a non-Gmail mailbox, go <a target=\"_blank\" href=\"http://www.inmotionhosting.com/support/email/email-troubleshooting/problems-receiving-email\">here</a>.</li>
<li>If you are using a Gmail mailbox, see this <a target=\"_blank\" href=\"https://www.youtube.com/watch?v=4vzPn5dxTOg\">video</a>.</li>
</ul>";
$string['amp_valpageintrotemplate_default'] = "<h1>EMAIL VALIDATION</h1>
<p>Before you can apply to become a @@ApplicationTypeDisplay@@ in the @@PackageTitle@@ program offered by @@OrgName@@...</p>
<p>...your email address must be validated. We do not want you to spend time filling out the full application, only to waste your effort because the email address you entered was mistyped or did not work.</p>
<p>Fill out the form below. After you submit it, you should receive an email message containing information to complete the @@ApplicationTitle@@ application process.</p>
<p>@@EVR_Form@@</p>
<p>If you do NOT receive the message, you must resolve your email issue first and fill out the form on this page again. Once you have received the email, you will have the instructions and the link which will continue the application process.</p>
<p>If you experience difficulty, please call @@OrgName@@ by telephone at @@OrgPhone@@ or send an email (via a different working address) to @@EmailAddrSender@@.</p>
<p>If you do not successfully validate your email address (by clicking on the link in your email validation message) within 30 days, your application attempt will be discarded.</p>";
$string['amp_mgmtemailaddr_default'] = '';

// Used by amp_EmailValidationIsEnabled()
$string['yesword'] = 'yes';
$string['noword'] = 'no';
$string['trueword'] = 'true';
$string['falseword'] = 'false';
$string['enabledword'] = 'enabled';
$string['disabledword'] = 'disabled';

// Used in applications.php
$string['applications'] = 'Applications';
