<?php

/**
 * These functions provide an interface to the OHF plugin management mechanism for this
 * plugin. For more details on how this mechanism works, see /local/ohflib/ohflib.php.
 *
 * ALL functions and classes in this module must have names prefixed with 'amp_'.
 *
 * Functions required in this module are:
 *		amp_getPluginInfo()
 *
 * @package   localamp
 * @copyright 2016 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


//**********************************************************************************************
/**
 * Returns an array of information about this plugin. Other information is added to it by
 * ohflib::ohflib_initialize(). Documentation of these fields can be found at
 * /local/ohflib/ohflib.php::ohflib_initialize().
 *
 * NOTE: All VarTags defined to be available in both the AMP and selected application package
 * plugin must be defined HERE, with the exception of VarTags defined in the application
 * package's [plugin-name]_application class (which should only ever be instantiated by AMP).
 * Any VarTags defined elsewhere in the application-package plugin risk clashing with those of
 * another installed but unselected application-package plugin.
 *
 * @return	array
 */
function amp_getPluginInfo() {

	$result = array(
		'path' => pathinfo( __FILE__, PATHINFO_DIRNAME ),
		'requires' => array(
			'ohf_library',
			'replace_text_vars'
		),
		'accepts' => array( 'application_pkg' ),
		'auto_setup' =>	TRUE,
		'org_code' => 'OHF',
		'org_name' => 'OneHundredFold',
		'usage' => 'application_manager',
		'desc' => 'Application and approval mechanism, optionally including validation of email addresses',
		'exclude_trans' =>	array(
			'javascript',
			'css',
			'images',
			'docs'
		),
		'setting_trans' =>	array(
			'amp_valpageintrotemplate',
			'amp_validationemailsubj',
			'amp_validationmsgtemplate',
			'amp_validationresponsepage'
		),
	);
	return $result;

} // end of function amp_getPluginInfo()


//**********************************************************************************************
/**
 * Invoked by OHFLIB only during plugin initialization. This is where all VarTag definitions
 * are made and other setup is done for this plugin.
 *
 * NOTE: This function is only called if the array returned by [plugin]_getPluginInfo()
 * contains the element 'auto_setup' => TRUE.
 *
 * @return	void
 */
function amp_setupPlugin() {

	global $CFG;

	$plugin_name = 'local_amp';
	$VT_MgrObj = new ohflib_VarTagManager();

	// ApplicantName - value set later
	$VT_MgrObj->createDefinition(
		$plugin_name,
		'ApplicantName',
		'Name of the applicant ("first-name family-name")',
		array(),
		array( 'application', 'amp' ),
		'[[ApplicantName]]'
	);

	// ApplicantEmailAddress - value set later
	$VT_MgrObj->createDefinition(
		$plugin_name,
		'ApplicantEmailAddress',
		'The email address declared by the applicant',
		array(),
		array( 'application', 'amp' ),
		'[[ApplicantEmailAddress]]'
	);

	// ApplicationComments - value set later
	$VT_MgrObj->createDefinition(
		$plugin_name,
		'ApplicationComments',
		'The comments an application reviewer made when accepting or declining an application',
		array(),
		array( 'application', 'amp' ),
		'[[ApplicationComments]]'
	);

	// DecisionDateTime - value set later
	$VT_MgrObj->createDefinition(
		$plugin_name,
		'DecisionDateTime',
		'The date/time an application decision was made (in various formats, eg. @@DecisionDateTime:2@@)',
		$VT_MgrObj->createParamInfoPkg(
			0, '1', '10 March 2001, 5:16 pm +02:00',
			0, '2', '10 March 2001',
			0, '3', '2001-03-10',
			0, '4', '2001-03-10 17:16:18'
		),
		array( 'application', 'amp' ),
		'[[DecisionDateTime]]'
	);

	// AssignedUsername - value set later
	$VT_MgrObj->createDefinition(
		$plugin_name,
		'AssignedUsername',
		"An accepted applicant's assigned username",
		array(),
		array( 'application', 'amp' ),
		'[[AssignedUsername]]'
	);

	// AssignedPassword - value set later
	$VT_MgrObj->createDefinition(
		$plugin_name,
		'AssignedPassword',
		"An accepted applicant's assigned password for first login",
		array(),
		array( 'application', 'amp' ),
		'[[AssignedPassword]]'
	);

	// SiteHomeURL
	$VT_MgrObj->createDefinition(
		$plugin_name,
		'SiteHomeURL',
		"The URL of the site's home page",
		array(),
		array( 'application', 'amp' ),
		$CFG->wwwroot
	);

	// EmailAddrSender
	$VT_MgrObj->createDefinition(
		$plugin_name,
		'EmailAddrSender',
		'The address from which validation emails are being sent',
		NULL,
		array( 'application', 'amp' ),
		$VT_MgrObj->createFunctionExecutorPkg( 'amp_getEmailFROMAddress' )
	);

	// EVR_Form
	$VT_MgrObj->createDefinition(
		$plugin_name,
		'EVR_Form',
		'Renders the Email Validation Request form',
		NULL,
		array( 'application', 'amp' ),
		$VT_MgrObj->createMethodExecutorPkg( 'amp_EVR_Manager', 'renderEVRForm' )
	);

	// EVR_FormData
	$VT_MgrObj->createDefinition(
		$plugin_name,
		'EVR_FormData',
		'Renders debug-formatted data submitted by the Email Validation form',
		$VT_MgrObj->createParamInfoPkg(
			0, '[echo]', 'true, 1 or yes = echo output to the screen',
			1, '[echo]', 'Any other value = do not echo)'
		),
		array( 'amp' ),
		$VT_MgrObj->createMethodExecutorPkg( 'amp_EVR_Manager', 'dumpFormData', array( 'true' ) )
	);

	// NumApplications
	$VT_MgrObj->createDefinition(
		$plugin_name,
		'NumApplications',
		'The number of applications defined in the selected application package',
		NULL,
		array( 'application' ),
		$VT_MgrObj->createFunctionExecutorPkg( 'amp_getNumberOfApplications' )
	);

	// ApplicationList
	$VT_MgrObj->createDefinition(
		$plugin_name,
		'ApplicationList',
		'A display of the applications offered by the currently-selected organization',
		NULL,
		array( 'amp' ),
		$VT_MgrObj->createFunctionExecutorPkg( 'amp_renderApplicationList' )
	);
	return;

} // end of function amp_setupPlugin()
