/**
 * Created by Ken_Wienecke on 4/26/2016.
 */

//**********************************************************************************************
// Displays the validation-request deletion-confirmation alert. If the deletion is confirmed,
// the browser is directed to the deletion URL. If the deletion is not confirmed, no action is
// taken.
//
// @param   string  prompt  The confirmation prompt to be displayed
// @param   string  url     Destination URL if the deletion is confirmed
//
function confirmDeletion(prompt, url) {
    if ( confirm( prompt ) ) {
        window.location.href = url;
    }
} // end of function confirmDeletion()


//**********************************************************************************************
// Returns a named value from the JavaScript Data Package (JDP) assumed to have been written
// as a JSON object into a hidden <div class='js_data_pkg'> in the page. The name (key)
// of each value in the package is addressed by this function's parameters as '<type>_<id>'.
//
// @param   string  type        'message' | 'variable'
// @param   string  id          EVR record ID
// @param   bool    required    TRUE = the element is required (issues error alert if not found)
//
// @return  string              Value of the specified variable
//
function getCtrlPkgElement(type, id, required) {
    var div = document.getElementById("js_data_pkg"),
        msgprefix = "Internal error: the JDP block 'js_data_pkg' ",
        name;
    if ( id == '' ) {
        name = type;
    } else {
        name = type + '_' + id;
    }
    // Test for the JDP
    if(div === null && typeof div === "object") {
        alert(msgprefix + "is required but not found on this page.");
        return null;
    }
    // The JDP exists on the page, so load it
    var errStrObj = JSON.parse(div.textContent),
        exists = errStrObj.hasOwnProperty(name);
    if (required && ! exists) {
        alert(msgprefix + "does not have required property '" + name + "'");
        return null;
    }
    if( exists ) {
        return errStrObj[name];
    } else {
        return null;
    }
} // end of function getCtrlPkgElement()


//**********************************************************************************************
// Displays any message (via a normal JavaScript alert) which might be pending in the JDP. Then,
// if a redirection URL was provided in the JDP the browser will redirect to it after the user
// closes the pending message. The purpose of this is to give the caller an opportunity to
// direct the browser back to a URL which will not have adverse effects (ie., taking an action)
// if the user refreshes the page.
//
function displayPendingPopup() {
    var pendingmsg = getCtrlPkgElement("message", "popup", false);
    // if(pendingmsg !== null && typeof pendingmsg !== "object") {
    //     alert(pendingmsg);
    // }
    if(pendingmsg !== null) {
        alert(pendingmsg);
    }
    var redirecturl = getCtrlPkgElement("variable", "redirecturl", false);
    // if(redirecturl !== null && typeof redirecturl !== "object") {
    //     window.location.href = redirecturl;
    // }
    if(redirecturl !== null) {
        window.location.href = redirecturl;
    }
} // end of function displayPendingPopup()


document.addEventListener('DOMContentLoaded', function() {
    // This code should fire after the DOM has been fully built
    displayPendingPopup();
}, false);