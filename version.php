<?php

/**
 * AMP - Application Management Plugin
 *
 * @package   local_application_mgt
 * @copyright 2014, Ken Wienecke <ken.w@100fold.it>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin_name->version   = 2016040103;	// 2016-04-01, build 3
$plugin_name->requires  = 2015051101;	// Moodle 2.9.1+ is required
$plugin_name->cron      = 0;				// cron is disabled
$plugin_name->component = 'local_amp';
$plugin_name->maturity  = MATURITY_STABLE;
$plugin_name->release   = '0.1';			// Inaugural version
$plugin_name->dependencies = array();
