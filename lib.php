<?php

/**
 * A number of functions invoked by Moodle directly. This library should not include any
 * functions other than those Moodle requires.
 *
 * @package   localamp
 * @copyright 2016 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */



//**********************************************************************************************
/**
 * Executes AMP actions when Moodle responds to a cron event.
 *
 * @return    void
 */
function local_amp_cron() {

	global $CFG;
	global $DB;

	//**********************************************************************************************
	// OHFLIB initialization - For details see <document_root>/local/ohflib/dev/initialize.php.
	$plugininfo = core_plugin_manager::instance()->get_plugin_info('local_ohflib');
	if ( is_null( $plugininfo ) ) {
		print_error( "The required plugin 'local_ohflib' is not installed. Contact your system administrator." ); // execution dies here
	} else {
		require_once $plugininfo->rootdir . "/ohflib.php";
		$PIMObj = new ohflib_PluginInfo_Manager( __FILE__ );
	}
	//**********************************************************************************************

	//return; // The following code is disabled but retained for future re-use

	echo "\nRemoving MMP temporary reports...";

	// Remove temporary report files after 15 minutes
	$files = scandir( $CFG->dirroot . '/local/mmp/temp/' );
	foreach ( $files as $file ) {
		if ( is_file( $CFG->dirroot . '/local/mmp/temp/' . $file ) ) {
			// TODO: Refactor the temp-file deletion interval to be setting-controlled
			if ( time() - filemtime( $CFG->dirroot . '/local/mmp/temp/' . $file ) > 900 ) {
				unlink( $CFG->dirroot . '/local/mmp/temp/' . $file );
			}
		}
	}

	// Check for completion of all quiz_grades modified recently
	$finishedTests = $DB->get_records_select( "quiz_grades", "timemodified > " . strtotime( '-1 day' ), NULL, "", "userid,timemodified" );
	foreach ( $finishedTests as $finishedTest ) {
		$courses = getGrades( $finishedTest->userid );
		$programCompleted = 1;
		foreach ( $courses as $course ) {
			foreach ( $course['lessons'] as $lesson ) {
				foreach ( $lesson['quizes'] as $quiz ) {
					if ( $quiz['required'] and !$quiz['grade'] ) {
						$programCompleted = 0;
					}
				}
			}
		}

		// Send email to administrator if all required quizes are completed
		$VT_MgrObj = new ohflib_VarTagManager();
		$return_email_address = $VT_MgrObj->filter( '@@EmailAddrSender@@' );
		$curriculum_name = $VT_MgrObj->filter( '@@PackageTitle@@' );
		if ( $programCompleted ) {
			if ( !$user_action = $DB->get_record_select( "mmp_user_actions", "userid = " . $finishedTest->userid . " AND action = 'program completed'", NULL, "userid" ) ) {
				$student = getUsers( $finishedTest->userid );
				$to = $return_email_address;
				$subject = "Program completion for " . $curriculum_name;
				$body = '
Student has completed all required quizes

Name: ' . $student[0]['lastname'] . ', ' . $student[0]['firstname'] . '

Email: ' . $student[0]['email'] . '

Student ID: ' . $student[0]['id'] . '

';
				// TODO: Refactor to use a settings template for this email - kaw
				$emailObj = new ohflib_EmailManager();
				$emailObj->setMessageFormat( 'plain' );
				$emailObj->addAddress( $to );
				$emailObj->setFrom( $return_email_address );
				$emailObj->setSubject( $subject );
				$emailObj->setBody( $body );
				$emailObj->transmit();

				$record = new stdClass();
				$record->id = NULL;
				$record->userid = $finishedTest->userid;
				$record->timesubmitted = strtotime( "now" );
				$record->timeapproved = 0;
				$record->action = 'program completed';
				$record->comments = '';
				$record->id = $DB->insert_record( 'mmp_user_actions', $record );
			}
		}
	}
	return;

} // end of function local_amp_cron()


//**********************************************************************************************
/**
 * Creates the root and branches of the "Applications" tree in the Moodle Navigation (not
 * Administration) menu. The menu links point to functionality offered by AMP. Some elements
 * offered depend on the logged-in user's role.
 *
 * @param	global_navigation $nav	Instance of the Moodle class global_navigation, defined
 *									in moodle/navigationlib.php.
 *
 * @return	void
 */
function local_amp_extend_navigation( global_navigation $nav ) {

	global $CFG, $PAGE;

	//**********************************************************************************************
	// OHFLIB initialization - For details see <document_root>/local/ohflib/dev/initialize.php.
	$plugininfo = core_plugin_manager::instance()->get_plugin_info('local_ohflib');
	if ( is_null( $plugininfo ) ) {
		print_error( "The required plugin 'local_ohflib' is not installed. Contact your system administrator." ); // execution dies here
	} else {
		require_once $plugininfo->rootdir . "/ohflib.php";
		$PIMObj = new ohflib_PluginInfo_Manager( __FILE__ );
	}
	//**********************************************************************************************

	$plugin_name = 'local_amp';
	$plugin_path = '/local/amp';
	$evr_mgt_url = $plugin_path . '/evrmanagement.php';
	// Application package plugin will add URL params to the following URLs
	$app_form_url = $plugin_path . '/introduction.php';
	$assoc_page_url = $plugin_path . '/page.php';
	$appPkg_Obj = amp_newApplicationPkgObj( __FUNCTION__, __LINE__ );
	$menu_elements = $appPkg_Obj->getNavigationMenuInfo( $app_form_url, $assoc_page_url );

	// Render the specified mix of application-related links
	if ( count( $menu_elements ) ) {
		// Some links are to be rendered, so instantiate the application package class which
		// defines all application types, forms, associated-page content and management
		// functionality.
		//
		// Each menu branch node must be created via reference to $PAGE->navigation, not a
		// derivative variable--otherwise the most recent created node of TYPE_CUSTOM will
		// obliterate nodes created by any other plugin

		// Create any application and page links
		if ( isset( $menu_elements['apps'] ) ) {
			// At least one application has links to render
			foreach( $menu_elements['apps'] as $app_id => $app_element_type ) {
				if ( isset( $app_element_type['form'] ) ) {
					// Render a link to the application form
					$form = $app_element_type['form'];
					$PAGE->navigation->add( $form['text'], $form['url'] );
				}
				if ( isset( $app_element_type['pages'] ) ) {
					// Render a link to the application form
					$pages = $app_element_type['pages'];
					foreach( $pages as $page_id => $page ) {
						$PAGE->navigation->add( $page['text'], $page['url'] );
					}
				}
			}
		}

		// Create management links
		$show_evr_link = ( $menu_elements['evrmgt_allowed'] and $CFG->amp_validateemail );
		if ( isset( $menu_elements['appmgt_link'] ) or $show_evr_link ) {
			// Create the 'Application' menu root and the links for...
			$amp_navs = $PAGE->navigation->add( get_string( 'amp', $plugin_name ) );
			// ...application management
			if ( isset( $menu_elements['appmgt_link'] ) ) {
				$link = $menu_elements['appmgt_link'];
				$amp_navs->add( $link['text'], $link['url'] );
				// TODO: Refactor this link-generation code to use the string engine
				$amp_navs->add( get_string( 'manage_cohorts', $plugin_name ), $CFG->wwwroot . '/cohort/index.php' );
				//$nodeMMPadminReports->add( get_string( 'application_list', 'local_mmp' ), new moodle_url( '/local/mmp/adminReports/applications.php' ) );
			}
			// ...EVR management
			if ( $show_evr_link ) {
				$amp_navs->add( get_string( 'introtitle', $plugin_name ), new moodle_url( $evr_mgt_url ) );
			}
		}

		//// Create 'My Cohort' links if required
		//if ( $appPkg_Obj->_role_Mgr->userHasRole( 'Non-editing Teacher' ) ) {
		//	// The current user is a cohort leader, so create the management links he/she requires
		//	$nodeMMP = $nav->add(get_string('my_cohort','local_mmp'));
		//	$nodeMMP->add(get_string('add_student','local_mmp'), new moodle_url('/local/mmp/application.php'));
		//	$nodeMMP->add(get_string('grade_book','local_mmp'), new moodle_url('/local/mmp/studentList.php'));
		//	$nodeMMP->add(get_string('exam_generation','local_mmp'), new moodle_url('/local/mmp/quizList.php'));
		//	$nodeMMP->add(get_string('remove_student','local_mmp'), new moodle_url('/local/mmp/removeStudent.php'));
		//	if ($CFG->mmp_uploadkey) {
		//		$nodeMMPSIS = $nodeMMP->add(get_string('sis_update','local_mmp'));
		//		if ($CFG->mmp_remote) {
		//			$nodeMMPSIS->add(get_string('courses','local_mmp'), new moodle_url('/local/mmp/sis/courses.php?push=1'));
		//		}
		//		$nodeMMPSIS->add(get_string('students','local_mmp'), new moodle_url('/local/mmp/sis/students.php?push=1'));
		//	}
		//	if ($CFG->mmp_remote) {
		//		$nodeMMPSYS = $nodeMMP->add(get_string('system','local_mmp'));
		//		$nodeMMPSYS->add(get_string('studentBackup','local_mmp'), new moodle_url('/local/mmp/studentBackup/studentBackup.php'));
		//		$nodeMMPSYS->add(get_string('shutdown','local_mmp'), new moodle_url('/local/mmp/sys/shutdown.php'));
		//	}
		//	$nodeMMP = $nav->add(get_string('logout','local_mmp'), new moodle_url('/login/logout.php?sesskey='.$_SESSION['USER']->sesskey));
		//}

		// Hide default navigation (Courses, Dashboard, etc)
		ohflib_hide_non_custom_nav_nodes();
	}

	// Generate navigation debugging output
	//ohflib_debugNavigation( get_string( 'pluginname', $plugin_name ) );
	return;

} // end of function local_amp_extend_navigation()
