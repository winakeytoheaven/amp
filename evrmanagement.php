<?php

/**
 * Implements management of email validation submissions, which includes these functions:
 *		A.	EVR Editing
 *		B.	EVR Deletion
 *		C.	Resending of EV message
 *		D.	Acknowledgement of an EV retransmission
 *
 * @package   localamp
 * @copyright 2016 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

global $CFG, $PAGE, $OUTPUT, $DB;

//**********************************************************************************************
// OHFLIB initialization - For details see <document_root>/local/ohflib/dev/initialize.php.
//**********************************************************************************************
require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';
$plugininfo = core_plugin_manager::instance()->get_plugin_info('local_ohflib');
if ( is_null( $plugininfo ) ) {
	print_error( "The required plugin 'local_ohflib' is not installed. Contact your system administrator." ); // execution dies here
} else {
	require_once $plugininfo->rootdir . "/ohflib.php";
	$PIMObj = new ohflib_PluginInfo_Manager( __FILE__ );
}
//**********************************************************************************************

// TODO: Add a sort-order mechanism to the table on this page - kaw

// Set up the page infrastructure
$PAGE->set_context(context_system::instance());
$params = array();
$PAGE->set_url('/local/amp/evrmanagement.php', $params);
$PAGE->set_pagelayout('frontpage');
$PAGE->blocks->add_region('content');
//$header = get_string('pluginname', 'local_amp');
$header = 'Manage EVRs';

$PAGE->set_title($header);
$PAGE->set_heading($header);
//$PAGE->requires->js( '/local/mmp/js/jquery-1.10.2.min.js', TRUE );
$PAGE->requires->js( '/local/amp/javascript/amp.js', TRUE );

// Get the action and validation request ID from the URL
$action = ohflib_getURLParam( 'a', '' );
$id = ohflib_getURLParam( 'id', -1 );

// Check for the appropriate user role and commandeer the action variable if not authorized
$role_Mgr = new ohflib_Role_Manager();
if ( ! $role_Mgr->userHasRole( array( "Manager" ) ) ) {
	$action = 'permission_error';
} else {
	// The user is authorized, so instantiate the EVR manager, which loads any
	// submitted EVR form data into the 'current' EVR. Be aware that amp_EVR_Manager()
	// will not be able to define the VarTag 'ApplicationType' because no specific
	// application is in focus
	$EVRMgrObj = new amp_EVR_Manager();
	if ( $EVRMgrObj->isLoaded() ) {
		// A new or edited EVR form was submitted, so save the data
		$EVRMgrObj->saveCurrent();
		$action = '';
	}
}

// Initialize a JDP for use on the page. Variables will be added as required by the specified action
// and the JDP will be rendered at the bottom of the page content.
$JDP_Obj = new ohflib_JDP_Manager();

if ( $action == 'resend' ) {
	// Resend the validation email, then redirect to a fresh EVR display page invoked
	// with a JavaScript alert acknowledging the resent email. The acknowledgement-page
	// URL will then have no URL parameters which could trigger another resend operation
	// if the page is refreshed by the user.
	$EVRMgrObj->setCurrent( $id );
	if ( $EVRMgrObj->errorOccurred() ) {
		// Invalid EVR ID
		$action = '';
	} else {
		// The ID is valid, so resend the validation email
		// $EVRMgrObj = new amp_EVR_Manager();
		$EVRMgrObj->sendValidationEmail( $CFG->amp_validationmsgtemplate );
		redirect( "evrmanagement.php?a=ack&id=$id" );
	}
}

if ( $action == 'delete' ) {
	// Delete the specified record. The user has already seen a JavaScript deletion
	// confirmation dialog from the previous page (which was attached to the DELETE
	// icons in the rendered EVR display) and confirmed the deletion. Once the
	// actual deletion is recognized and deleted here, the browser will be redirected
	// to a simple display of the remaining EVRs with no URL parameters which could
	// request another deletion if the page were refreshed.
	$EVRMgrObj->setCurrent( $id );
	$EVRMgrObj->deleteCurrent();
	redirect( "evrmanagement.php" );
}

// The previous actions redirect the browser to a parameter-less URL, which simply renders
// the EVR table. This had to happen BEFORE any output was generated because redirection
// headers must be . The following actions
// will create output, so generate the page-framework output now.
echo $OUTPUT->header();
echo $OUTPUT->blocks_for_region('content');

if ( $action == 'permission_error' ) {
	// Show the permission error
	echo get_string( 'not_authorized_page', 'local_ohflib' );
}

if ( $action == 'ack' ) {
	// Display an acknowledgement message contained in the JS package
	$EVRMgrObj->setCurrent( $id );
	$stdObj = new stdClass();
	$stdObj->name = $EVRMgrObj->getApplicantName();
	$JDP_Obj->setVariable( 'message_popup', get_string( 'resendacknowledgement', 'local_amp', $stdObj ) );
	$action = '';
}

if ( $action == 'edit' ) {
	// Edit the specified request
	$EVRMgrObj->setCurrent( $id );
	$name = $EVRMgrObj->getApplicantName();
	echo "<h1>Editing the Email Validation Request (EVR) for $name</h1>";
	echo $EVRMgrObj->renderEVRForm();
}

if ( $action == '' ) {
	// Render render the display of validation requests
	echo "\n<h1>Manage Email Validation Requests</h1>";
	echo
		"\n<p>An Email Validation Request (EVR) is launched when a user initiates an application process. "
		. "This site then sends the applicant an email (containing a link to the applicant-chosen application), "
		. "an action recorded here as an EVR. After receiving the email, the applicant clicks on the link "
		. "in that email. This should open the applicant's browser to an address which 'validates' the email address (see the "
		. "'Time Validated' column below) and displays the application form for the study program or position requested.</p>"
		. "\n<p>This is where you view and manage EVRs. You may delete or edit them, or resend an EVR validation email.</p>"
		. "\n<p>Email validation is currently " . ohflib_wrapWithSpan( amp_EmailValidationIsEnabled( 'enabled', 'upper' ), 'green') . "</p>";
	$JDP_Obj->setVariable( 'message_confirmdelprompt', get_string( 'confirmdelprompt', 'local_amp' ) );
	$EVRMgrObj->renderEVRDisplay();
}

// Render the JDP and the footer
echo $JDP_Obj->render();
echo $OUTPUT->footer();
exit;
