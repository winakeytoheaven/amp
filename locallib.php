<?php
/**
 * A number of functions invoked only by the AMP plugin.
 *
 * @package   localamp
 * @copyright 2016 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

global $CFG;


//**********************************************************************************************
/**
 * Returns TRUE if the current user is a Guest or otherwise not logged in.
 *
 * @return	boolean
 */
function amp_roleIsGuestOrNotLoggedIn() {

	$id = $_SESSION['USER']->id;
	$result = ( is_null( $id ) || $id == 0 );
	return $result;

} // end of function amp_roleIsGuestOrNotLoggedIn()


//**********************************************************************************************
/**
 * Returns an option list of plugin names for all installed plugins of usage 'application_pkg'.
 * Each entry of the option list array has the structure '[plugin_name]' => '[app_title]' and
 * can be used to build selection controls in a form.
 *
 * @param	string	$caller		The source file of the caller
 * @param	int		$line		The line number of the caller
 *
 * @return	array
 */
function amp_buildApplicationPkgOptionList( $caller, $line ) {

	$result = array();
	$plugin_list = ohflib_getPluginsOfUsage( 'application_pkg' );
	foreach( $plugin_list as $plugin_name ) {
		$appObj = amp_newApplicationPkgObj( $caller, $line, $plugin_name );
		$pkg_title = $appObj->getPackageName();
		$result[$plugin_name] = $pkg_title;
	}
	return $result;

} // end of function amp_buildApplicationPkgOptionList()


//**********************************************************************************************
/**
 * Renders the Email Validation form via the EVR Manager class.
 *
 * @return	string		Form HTML
 */
function amp_renderEmailValidationForm() {

	$EVRMgrObj = new amp_EVR_Manager();
	// Render the form
	$result = $EVRMgrObj->renderEVRForm();
	return $result;

} // end of function amp_renderEmailValidationForm()


//**********************************************************************************************
/**
 * Renders the Email Validation form via the EVR Manager class.
 *
 * @return	string		Form HTML
 */
function amp_renderEmailValidationFormData() {

	// Instantiate the email validation form class
	$validation_form = new amp_EVR_Form();
	// Get the form data
	$form_data = $validation_form->get_data();
	if ( is_null( $form_data ) ) {
		// The form's fields did not validate
		$html = "This form has not been submitted.";
	} else {
		$form_data_array = (array)$form_data;
		$html = ohflib_dumpVar( "form_data_array", $form_data_array );
	}
	return $html;

} // end of function amp_renderEmailValidationFormData()


//**********************************************************************************************
/**
 * Used to create an error message redirect page.
 *
 * @param	string	$return_url		Target URL of the redirect
 * @param	string	$error_code		Code matching a string value in /amp/lang/<lang>/local_amp.php
 * @param	object	$var_object		stdClass object of parameters destined for insertion into the message
 * @param	integer	$delay			Time to delay before redirecting, -1 = wait for user to continue
 *
 * @throws coding_exception
 * @throws moodle_exception
 */
function amp_printAndRedirect( $return_url, $error_code, $var_object, $delay = -1 ) {

	$message = get_string( $error_code, 'local_amp', $var_object );
	redirect( $return_url, $message, $delay ); // execution stops here

} // end of function amp_printAndRedirect()


//**********************************************************************************************
/**
 * This function is part of the smoke and mirrors required to tie into existing MMP application
 * modules. This will disappear when AMP is complete.
 *
 * @param	string	$mmp_module		Name of the MMP module (plus any needed URL params)
 *
 * @return	string					URL
 */
function amp_makeLinkToMMP( $mmp_module ) {

	$result = ohflib_getCurrentURLHost() . "/local/mmp/$mmp_module";
	return $result;

} // end of function amp_makeLinkToMMP()


//**********************************************************************************************
/**
 * This function instantiates the 'application' class of the currently-specified application
 * package and returns the class object. If there is no currently-specified application package,
 * the AMP-defined EmptyAppPkg application class is instantiated and returned. Any other error
 * displays a Moodle-formatted message and halts execution.
 *
 * @param	string	$function			Name of the calling function
 * @param	integer	$line				Line number in the calling function
 * @param	string	$pkg_plugin_name	Name of the application-package plugin ([type]_[name])
 *
 * @return	object
 */
function amp_newApplicationPkgObj( $function = __FUNCTION__, $line = __LINE__, $pkg_plugin_name = '' ) {

	global $CFG;

	if ( $pkg_plugin_name == '' ) {
		// Package plugin not provided, so use the one from the AMP setting amp_pkgselection
		$pkg_plugin_name = ( isset( $CFG->amp_pkgselection ) ) ? $CFG->amp_pkgselection : '';
	}
	if ( $pkg_plugin_name == '' ) {
		// There was no specified package, and no current package selection, so instantiate
		// AMP's EmptyAppPkg class
		$app_pkg_class_file = ohflib_getPluginDirectory( 'local_amp' ) . "/classes/class.EmptyAppPkg.php";
		include_once $app_pkg_class_file;
		$pkg = new EmptyApplicationPackage();
	} else {
		// A package is specified
		$pkg = ohflib_new( $function, $line, $pkg_plugin_name, 'applications' );
		if ( ! ohflib_callOK() ) {
			// An error occurred instantiating the specified package 'applications' class, so
			// display a message and die
			print_error( ohflib_callGetErrMsg() );
		}
	}
	return $pkg;

} // end of function amp_newApplicationsObj()


//**********************************************************************************************
/**
 * Returns the number of selectable applications defined for the current-selected organization.
 * $mode determines whether an integer or string is returned.
 *
 * @param	string	$mode	'string' returns a string
 *							'int' or 'integer' returns an integer
 *
 * @return	int|string
 */
function amp_getNumberOfApplications( $mode = 'string' ) {

	$appObj = amp_newApplicationPkgObj();
	// Instantiation worked
	$result = $appObj->getNumApplications( $mode );
	return $result;

} // end of function amp_getNumberOfApplications()


//**********************************************************************************************
/**
 * Returns the name of the organization affiliated with the currently-selected application
 * package.
 *
 * @return	string
 */
function amp_getOrganizationName() {

	$appObj = amp_newApplicationPkgObj();
	return $appObj->getOrganizationName();

} // end of function amp_getOrganizationName()


//**********************************************************************************************
/**
 * Returns the name of the currently-selected application package.
 *
 * @return	string
 */
function amp_getPackageName() {

	$appObj = amp_newApplicationPkgObj();
	return $appObj->getPackageName();

} // end of function amp_getPackageName()


//**********************************************************************************************
/**
 * This function returns the settings-selected email FROM address.
 */
function amp_getEmailFROMAddress() {

	global $CFG;

	$result = isset( $CFG->amp_validationemailaddr ) ? $CFG->amp_validationemailaddr : '';
	return $result;

} // end of function amp_getEmailFROMAddress()


//**********************************************************************************************
/**
 * Returns the list of applications for the currently-selected application-package plugin.
 *
 * @param    string $mode	'plain' = render plain-text version of applications and links
 *							'html' = render the application(s) with links
 *							'data' = return application list data
 *
 * @return	string
 */
function amp_renderApplicationList( $mode = 'plain' ) {

	$appObj = amp_newApplicationPkgObj();
	$result = $appObj->renderApplicationList( $mode );
	return $result;

} // end of function amp_renderApplicationList()


//**********************************************************************************************
/**
 * Returns the current email validation enablement status as a boolean or two forms of
 * string representations. Returned string representations can have 3 formats.
 *
 * @param	string	$format	'boolean' | 'yes' | 'true' | 'enabled'
 * @param	string	$case	'lower' | 'upper' | 'initial'
 *
 * @return	bool|string
 */
function amp_EmailValidationIsEnabled( $format = 'boolean', $case = 'initial' ) {

	global $CFG;

	$plugin_name = 'local_amp';
	$result = ohflib_makeBooleanFromString( $CFG->amp_validateemail );
	if ( $format <> 'boolean' ) {
		switch( $format ) {
			case "yes":
				$result = ( $result ) ? get_string( 'yesword', $plugin_name ) : get_string( 'noword', $plugin_name );
				break;
			case "true":
				$result = ( $result ) ? get_string( 'trueword', $plugin_name ) : get_string( 'falseword', $plugin_name );
				break;
			case "enabled":
				$result = ( $result ) ? get_string( 'enabledword', $plugin_name ) : get_string( 'disabledword', $plugin_name );
		}
		switch( $case ) {
			case "lower":
				$result = strtolower( $result );
				break;
			case "upper":
				$result = strtoupper( $result );
				break;
			case "initial":
				$result = strtolower( $result );
				$result = ucfirst( $result );
		}
	}
	return $result;

} // end of function amp_EmailValidationIsEnabled()


//**********************************************************************************************
/**
 * Creates or updates a record in this plugin's email validation table.
 *
 * @param	array	$form_data_array		Associative array of record fields/values
 * @param	bool	$ok_to_set_validation	TRUE = update the validation timestamp to now
 *
 * @return	bool|int						boolean = result of update, integer = new record ID
 */
function amp_save_validation_data( $form_data_array, $ok_to_set_validation = FALSE ) {

	global $DB;

	$table_name = "amp_email_validations";
	$record = new stdClass();
	$record->first_name = $form_data_array['fname'];
	$record->last_name = $form_data_array['lname'];
	$record->phone = $form_data_array['phone'];
	$record->country_code = $form_data_array['country_code'];
	$record->email_address = $form_data_array['email'];
	if ( $ok_to_set_validation ) {
		$record->validated = time();
	} else {
		$record->validated = 0;
	}
	if ( isset( $form_data_array['id'] ) ) {
		// This is a record update
		$record->id = $form_data_array['id'];
		$result = $DB->update_record( $table_name, $record );
	} else {
		// This is a new record creation
		$record->requested = time();
		$result = $DB->insert_record( $table_name, $record, TRUE );
	}
	return $result;

} // end of function amp_save_validation_data()


//**********************************************************************************************
/**
 * Transmits the email validation response message to the address provided in the submitted
 * form data.
 *
 * @param	array	$form_data		Data from the submitted form
 * @param	string	$email_body		HTML content of the email to be sent
 *
 * @return	void
 */
function amp_send_validation_email( $form_data, $email_body ) {

	global $CFG;

	if ( ohflib_runningOnline() ) {
		$emailObj = new ohflib_EmailManager();
		$emailObj->setMessageFormat( 'plain' );
		$emailObj->addAddress( $form_data['email'] );
		$emailObj->setFrom( $CFG->amp_validationemailaddr );
		$emailObj->setSubject( get_string( 'application_for', 'local_btsccm' ) );
		$emailObj->setBody( $email_body );
		$emailObj->transmit();
	}
	return;

} // end of function amp_send_validation_email()


//**********************************************************************************************
/**
 * Returns the currently-defined address to be used for management notifications related
 * to events in application process.
 *
 * @return	string
 */
function amp_getAppMgmtNotificationAddr() {

	global $CFG;

	$result = isset( $CFG->amp_mgmtemailaddr ) ? $CFG->amp_mgmtemailaddr : '';
	return $result;

} // end of amp_getAppMgmtNotificationAddr()
