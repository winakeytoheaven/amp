<?php

/**
 * Shows the response to form submissions for development purposes.
 *
 * @package   localamp
 * @copyright 2016 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

global $CFG, $PAGE, $OUTPUT;

//**********************************************************************************************
// OHFLIB initialization - For details see <document_root>/local/ohflib/dev/initialize.php.
//**********************************************************************************************
require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';
$plugininfo = core_plugin_manager::instance()->get_plugin_info('local_ohflib');
if ( is_null( $plugininfo ) ) {
	print_error( "The required plugin 'local_ohflib' is not installed. Contact your system administrator." ); // execution dies here
} else {
	require_once $plugininfo->rootdir . "/ohflib.php";
	$PIMObj = new ohflib_PluginInfo_Manager( __FILE__ );
}
//**********************************************************************************************

// Include the required modules--the current working directory is this plugin's folder
require_once( '../locallib.php' );

// Set up the page infrastructure
$PAGE->set_context(context_system::instance());
$params = array();
$PAGE->set_url('/local/amp/dev/showformdata.php', $params);
$PAGE->set_pagelayout('frontpage');
$PAGE->blocks->add_region('content');
//$header = get_string('pluginname', 'local_amp');
$header = 'Form Development';

$PAGE->set_title($header);
$PAGE->set_heading($header);

// Include the base class moodleform (defined in formslib.php) and the email validation form class
require_once( $CFG->libdir . "/ohflib/ohflib.php" );
echo $OUTPUT->header();
echo $OUTPUT->blocks_for_region('content');

$role_Mgr = new ohflib_Role_Manager();
if ( $role_Mgr->userHasRole( array( 'Manager' ) ) ) {
	echo "<h1>Submitted form data</h1>";
	if ( isset( $_REQUEST ) ) {
		$form_data_array = (array)$_REQUEST;
		ohflib_dumpVar( '_REQUEST', $form_data_array, TRUE );
	} else {
		echo "There was no submitted form data.";
	}
} else {
	echo get_string( 'not_authorized_page', 'local_ohflib' );
}

echo $OUTPUT->footer();
