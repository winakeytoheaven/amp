<?php

/**
 * Manages the presentation, rendering and parsing of the selected application as well as the
 * management of (as defined by an application-definition plugin selected by the AMP plugin
 * settings).
 *
 * NOTE: This class is developmental - kaw
 *
 * @package   localohflib
 * @copyright 2016 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class amp_ApplicationManager {

	private $_data_package = array();

	//**********************************************************************************************
	/**
	 * This is the class constructor.
	 */
	function __construct() {

		return;

	} // end of constructor


	//**********************************************************************************************
	/**
	 * Creates a variable/value pair in the data package.
	 *
	 * @param	string	$name	Name of the JS variable (must conform to JS variable naming rules)
	 * @param	mixed	$value	Value of the variable
	 *
	 * @return	void
	 */
	function setVariable( $name, $value ) {

		$this->_data_package[ $name ] = $value;
		return;

	} // end of method setVariable()


	//**********************************************************************************************
	/**
	 * Renders a hidden DIV element with the classname 'js_data_pkg' which contains the provided
	 * data destined for use by JavaScript. The data will be encoded as JSON before being rendered
	 * in the DIV element, and it must be accessed via the JS function getCtrlPkgElement(type, id),
	 * where
	 *
	 *		type =	message | variable
	 *		id =	variable name by which getCtrlPkgElementName returns a value
	 *
	 * The keys are formed as '<type>_<name>', where
	 *		<type> = 'message' | 'variable' and <name> is determined by the amp.js.
	 *
	 * @return string
	 */
	function renderApplication() {

		//ohflib_dumpVar( '$js_data', $js_data );
		$html = "\n<div id='js_data_pkg' style='display:none'>" . json_encode( $this->_data_package ) . "</div>";
		return $html;

	} // end of method renderApplication()

} // end of class amp_ApplicationManager
