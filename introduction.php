<?php
/**
 * This module
 *		A.	Processes submission of the email validation form by either showing any error
 *			or rendering the specified application form
 *		B.	If validation is enabled, displays the application-process introduction page
 *			appropriate for the specified application type (ie., 'student')
 *		C.	Displays the acknowledgement of a submitted EVR form.
 *
 * Be aware that the URL parameter 'apptype' must specified a valid type during all of the
 * functions above.
 *
 * @package   localamp
 * @copyright 2016 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

global $CFG, $PAGE, $OUTPUT;
require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

//**********************************************************************************************
// OHFLIB initialization - For details see <document_root>/local/ohflib/dev/initialize.php.
//
$plugininfo = core_plugin_manager::instance()->get_plugin_info('local_ohflib');
if ( is_null( $plugininfo ) ) {
	print_error( "The required plugin 'local_ohflib' is not installed. Contact your system administrator." ); // execution dies here
} else {
	require_once $plugininfo->rootdir . "/ohflib.php";
	$PIMObj = new ohflib_PluginInfo_Manager( __FILE__ );
}
//**********************************************************************************************

$local_amp = 'local_amp';

// Set up the page infrastructure
$PAGE->set_context(context_system::instance());
$params = array();
$PAGE->set_url('/local/amp/introduction.php', $params);
$PAGE->set_pagelayout('frontpage');
$PAGE->blocks->add_region('content');
$header = get_string('introtitle', $local_amp);
$PAGE->set_title($header);
$PAGE->set_heading($header);

// Controls the output of debug output from an email transmission
$ok_to_debug = ohflib_getURLParam( 'debug', '0' );
$ok_to_debug = ( $ok_to_debug == '1' ) ? TRUE : FALSE;

// Get the application type (which might ultimately not be used if not required)
$app_type = ohflib_getURLParam( 'apptype', NULL );

// Instantiate the VarTag manager and create the ApplicationType vartag (used by
// the class amp_EVR_Form, which is invoked from at least two places having no clean
// way to pass the application type value)
$VT_MgrObj = new ohflib_VarTagManager();
$desc = 'The application type';
$VT_MgrObj->setVarTagValue( 'ApplicationType', $app_type, $desc, array( 'application' ) );
$app_type_display = ucwords( str_replace( '_', ' ', $app_type ) );
$desc = 'The displayable application type';
$VT_MgrObj->setVarTagValue( 'ApplicationTypeDisplay', $app_type_display, $desc, array( 'application' ) );

// Instantiate the EVR manager, which loads any submitted EVR form data into the
// 'current' EVR
$EVRMgrObj = new amp_EVR_Manager();
if ( $EVRMgrObj->errorOccurred() ) {
	// amp_EVR_Manager flags this error only if $app_type is actually needed
	// during the creation of a new record and proves to be invalid
	print_error( 'The URL parameter "apptype=[application-id]" is required but not found.' ); // execution dies here
}

// Get the validation override (which overrides the value of $CFG->amp_validateemail)
$ev_override = ohflib_getURLParam( 'ev', NULL );
$CFG->amp_validateemail = is_null( $ev_override ) ? $CFG->amp_validateemail : $ev_override;
if ( amp_EmailValidationIsEnabled() ) {
	// Email validation is enabled, so check for a submitted EVR form
	if ( $EVRMgrObj->isLoaded() ) {
		// A form was submitted, so select the application type submitted with it
		$app_type = $EVRMgrObj->getCurrentAppType();
		if ( $EVRMgrObj->isDuplicate() ) {
			// The submitted form contains data which is a duplicate of a previous request,
			// so set up page content to indicate it
			$page_content = "This request duplicates a previous request. Please contact @@OrgName@@.";
		} else {
			// The submitted form does not duplicate anything in the database, so save the data
			$EVRMgrObj->saveCurrent();
			$evrid = $EVRMgrObj->getCurrentId();
			// Transmit the validation email
			$snapshot = $EVRMgrObj->sendValidationEmail( $CFG->amp_validationmsgtemplate, $ok_to_debug );
			$id = $EVRMgrObj->getCurrentId();
			if ( $ok_to_debug ) {
				$page_content = $snapshot;
			} else {
				// Send the user to a 'thank-you' page (redirect to a fresh page to avoid the browser's
				// dialog about resubmitting the data if the page is refreshed
				redirect( "introduction.php?a=ack&id=$id&apptype=$app_type" ); // flow ends here
			}
		}
	} else {
		// No form was submitted, so set up page content according to the specified action
		$action = ohflib_getURLParam( 'a' );
		if ( $action == '' ) {
			// Display the email introduction page (presumably including the EVR form)
			$page_content = $CFG->amp_valpageintrotemplate;
		}
		if ( $action == 'ack' ) {
			$id = ohflib_getURLParam( 'id', NULL );
			$EVRMgrObj->setCurrent( $id );
			$VT_MgrObj->setVarTagValue( 'ApplicantName', $EVRMgrObj->getApplicantName() );
			$page_content = $CFG->amp_validationresponsepage;
		}
	}
} else {
	// Email validation is disabled, so redirect to the specified application form
	$url = $VT_MgrObj->getVarTagValue( 'ApplicationURL' );
	redirect( $url ); // flow ends here
}

// Every action past this point renders a page
echo $OUTPUT->header();
echo $OUTPUT->blocks_for_region( 'content' );

echo $VT_MgrObj->filter( $page_content );

// Finish up the page output
echo $OUTPUT->footer();
