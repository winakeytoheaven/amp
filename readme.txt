The Application Management Plugin (AMP) for Moodle

This plugin implements an application process for Moodle users. The business flow is:

	1.	From the Moodle home page, the aspiring user clicks on a configuration-defined
		link which either presents
			A.	Email verification is ON: a minimal form prompting for name, email address
				and phone number. The displayed page also displays text (defined in the
				plugin's settings) which explains why and how the email address value must
				be validated through a message sent to that address. Go to step 2.
			B.	Email verification is OFF: A page which displays the instructions for, and a
				link to, each defined application form. Once the user clicks on the desired
				application and it is rendered, go to step 5.
	2.	The user fills out and submits the form, triggering an email message to the supplied
		address. The message body contains one or more links to entry points in this plugin.
		Each link represents an application mechanism for a specific kind of Moodle user. The
		receipt of the email message proves that the supplied address is valid.
	3.	For	example, an aspiring student would click the "student" application link in the
		validation email. That link points to this plugin and carries parameters which identify
		the email recipient and the desired application. More than one kind of application can
		be defined by this plugin.
	4.	Responding to the clicked email link, this plugin validates the link parameters and
		displays the specified application form. Each organization, its applications and its
		business-process settings are defined in separate Moodle local plugins named
		"<orgcode><programcode>" where
			<orgcode> = Organization code: "bts", etc
			<programcode> = Program code: "CCM", "MATS", etc
	5.	The aspiring Moodle user then fills out and submits the rendered application form. The
		form is preloaded with the validated (and non-editable) email address if email valida-
		tion was done.
	6.	This plugin responds to the submitted form by saving the form content in its own
		table. It also renders a Navigation menu item which will allow the submitted forms
		to be viewed and processed by the organization staff members who evaluate submitted
		applications.
	7.	This plugin produces several displays of application information. When an application
		is approved, a confirming email is sent to the applicant and a suitably-empowered
		Moodle account is created. If an application is denied, an email to that effect is
		sent to the applicant.
