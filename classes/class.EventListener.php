<?php
/**
 * Created by PhpStorm.
 * User: ken_wienecke
 * Date: 8/5/16
 * Time: 2:43:52 PM
 */

//namespace ohf_family_amp;

defined('MOODLE_INTERNAL') || die();

class amp_event_listener {

	//**********************************************************************************************
	/**
	 * Event processor for the 'core\event\course_viewed' event
	 */
	public static function course_viewed( \core\event\course_viewed $event ) {
		return true;
	}

} // end of class amp_event_listener
