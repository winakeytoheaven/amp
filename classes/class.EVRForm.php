<?php
/**
 * This class implements these parts of the AMP email validation request (EVR) mechanism:
 *		Form definition
 *		Form rendering
 *		Validation of submitted form data
 *
 * Though the form's action URL can be set by the calling code, by default this class always
 * presumes that the form will be set up to submit to the same URL active when it was
 * instantiated. In other words, the current page URL will receive the form's submitted data.
 *
 * Also, this class requires that the VarTag 'ApplicationType' (see class ohflib_VarTagManager)
 * be defined with a valid value prior to instantiation. The EVR form created by this class
 * must have that value when creating an empty form, and the cleanest way to pass it reliably
 * is via the VarTag mechanism. This class is used in both the class amp_EVR_Manager and the
 * ohfvartags filter plugin.
 *
 * @package   localamp
 * @copyright 2016 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

global $CFG;
require_once $CFG->libdir . "/formslib.php";

/**
 * Class amp_EVR_Form
 *
 * This class creates the Email Validation Request (EVR) form, renders it, receives data submitted from
 * it, and validates it.
 */
class amp_EVR_Form extends moodleform {

	//**********************************************************************************************
	/**
	* Constructor.
	*
	* @param	mixed	$action		The action attribute for the form. If empty defaults to auto detect the
	*                               current URL. If this is a moodle_url object, then parameters are rendered
	*                               in the form as hidden variables
	* @param	mixed	$customdata	If your form definition method needs access to data such as $course
	*								$cm, etc. to construct the form definition then pass it in this array. You can
	*								use globals for somethings
	* @param	string	$method		If you set this to anything other than 'post' then _GET and _POST will
	*								be merged and used as incoming data to the form
	* @param	string	$target		Target frame for form submission. You will rarely use this. Don't use
	*								it if you don't need to as the target attribute is deprecated in xhtml strict
	* @param	mixed	$attributes	HTML attributes, as a string or an array
	* @param	bool	$editable	TRUE = The form attributes can be edited, FALSE = form attributes are frozen
	*/
	function __construct( $action=null, $customdata=null, $method='post', $target='', $attributes=null, $editable=true ) {


		parent::moodleform( $action, $customdata, $method, $target, $attributes, $editable );

		return;

	} // end of constructor


	//**********************************************************************************************
	/**
	 * Adds element definitions:
	 *
	 * For details about the addElement() parameters, see
	 *
	 *		lib/pear/HTML/QuickForm.php
	 *
	 * @throws coding_exception
	 */
	public function definition() {

		$local_amp = 'local_amp';
		$mform = $this->_form; // Don't forget the underscore!

		// Header
		//$mform->addElement('header', 'general', 'This is the header');

		// Define the ID field (hidden)
		$name = 'id';
		$mform->addElement('hidden', $name, '-1');
		$mform->setType($name, PARAM_NOTAGS);

		// Define the first name field
		$name = 'first_name';
		$label = get_string('evffnamelabel', $local_amp);
		$mform->addElement('text', $name, $label, array( 'class' => 'required', 'size' => 20 ) );
		$mform->setType($name, PARAM_RAW_TRIMMED);
		// Add rule to require element content
		$params = new stdClass();
		$params->label = $label;
		$message = get_string( 'evfblankerror', $local_amp, $params );
		$mform->addRule( $name, $message, 'required', NULL, 'client' );

		// Define the last name field
		$name = 'last_name';
		$label = get_string('evflnamelabel', $local_amp);
		$mform->addElement('text', $name, $label, array( 'class' => 'required', 'size' => 20 ) );
		$mform->setType($name, PARAM_RAW_TRIMMED);
		// Add rule to require element content
		$params = new stdClass();
		$params->label = $label;
		$message = get_string( 'evfblankerror', $local_amp, $params );
		$mform->addRule( $name, $message, 'required', NULL, 'client' );

		// Define the country field
		$name = 'country_code';
		$label = get_string('evfcountrylabel', $local_amp);
		$countriesObj = new ohflib_countries();
		//$countriesObj = ohflib_new( __FILE__, __LINE__, 'ohflib', 'countries' );
		$countries = $countriesObj->getCountryOptionList();
		$mform->addElement('select', $name, $label, $countries );
		$mform->addHelpButton($name, 'evfcountry', $local_amp);

		// Define the email address field
		$name = 'email_address';
		$label = get_string('evfemaillabel', $local_amp);
		$mform->addElement('text', $name, $label, array( 'class' => 'required', 'size' => 40 ) );
		$mform->setType($name, PARAM_NOTAGS);
		// Add rule to require element content
		$message = get_string( 'evfemailerror', $local_amp, $params );
		$mform->addRule( $name, $message, 'required', NULL, 'client' );
		$mform->addRule( $name, $message, 'email', NULL, 'client' );

		// Define the hidden application_type field
		$name = 'application_type';
		//$mform->addElement('text', $name, "Application Type");
		$mform->addElement('hidden', $name, '');
		$mform->setType($name, PARAM_RAW);
		// The default value is taken from a VarTag
		$VTMgr = new ohflib_VarTagManager();
		$value = $VTMgr->getVarTagValue( 'ApplicationType' );
		$mform->setDefault($name, $value );

		// Define the hidden time_requested field
		$name = 'time_requested';
		//$mform->addElement('text', $name, "Time Requested");
		$mform->addElement('hidden', $name, '0');
		$mform->setType($name, PARAM_INT);
		$mform->setDefault($name, 0);

		// Define the hidden time_validated field
		$name = 'time_validated';
		//$mform->addElement('text', $name, "Time Validated");
		$mform->addElement('hidden', $name, '0');
		$mform->setType($name, PARAM_INT);
		$mform->setDefault($name, 0);

		$this->add_action_buttons( FALSE, get_string( 'evfsubmit', $local_amp ) );
		return;

	} // end of method definition()


	//**********************************************************************************************
	/**
	 * Returns a simple array of element names which are required in the form.
	 *
	 * @return	array
	 */
	function namesOfRequiredElements() {

		return $this->_form->_required;

	} // end of method namesOfRequiredElements()


	//**********************************************************************************************
	/**
	 * Returns a simple array of element names which are required in the form.
	 *
	 * @return	array
	 */
	function getValidationErrors() {

		return $this->_form->_errors;

	} // end of method getValidationErrors()


	//**********************************************************************************************
	/**
	 * Returns an HTML rendering of validation error messages. Multiple errors returns a formatted
	 * unordered list.
	 *
	 * @return	string
	 */
	function renderValidationErrors() {

		$html = '';
		$num_errors = count( $this->_form->_errors );
		if ( $num_errors > 1 ) $html = "\n<ul>";
		foreach( $this->_form->_errors as $field_name => $message ) {
			if ( $num_errors > 1 ) $html = "\n\t<li>";
			$html .= "$field_name: $message";
			if ( $num_errors > 1 ) $html = "</li>";
		}
		if ( $num_errors > 1 ) $html = "\n</ul>";
		return $html;

	} // end of method renderValidationErrors()


	//**********************************************************************************************
	/**
	 * Returns TRUE if the form validates.
	 *
	 * @return	boolean
	 */
	function isValidated() {

		return $this->_form->validate();

	} // end of method isValidated()


	//**********************************************************************************************
	/**
	 * Clears out any data that might have been submitted by the form. This is necessary
	 * because form-submitted data takes precedence such that set_data() has no effect.
	 */
	function clearFormData() {

		$this->_form->_submitValues = array();
		return;

	} // end of method clearFormData()

} // end of class amp_EVR_Form
