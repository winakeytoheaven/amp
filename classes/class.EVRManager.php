<?php

/**
 * This class performs all the high-level operations of managing the Email Validation Request (EVR)
 * mechanism:
 *		Render the EVR table display
 *		Render the EVR create/edit form
 *		Create an new EVR
 *		Edit an existing EVR
 *		Delete an existing EVR
 *		Send the EVR email
 *
 * Upon instantiation, any form data that was submitted is made the current EVR, whether or not it
 * it is valid. Thus a caller can invoke isLoaded() after instantiating and a TRUE result means that
 * the submitted form data is now the current EVR.
 *
 * Also, this class requires that the VarTag 'ApplicationType' (see class ohflib_VarTagManager)
 * be defined with a valid value prior to instantiation.
 *
 * @package   localamp
 * @copyright 2016 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class amp_EVR_Manager {

	/**
	 * Application type specified at instantiation.
	 *
	 * @var	string
	 */
	private $_application_type = '';

	/**
	 * Name of the Email Request Validation table.
	 *
	 * @var	string
	 */
	private $_EVR_table_name = 'amp_email_validations';

	/*
	 * The EVR current record, or empty array if no record is current.
	 *
	 * @var	array
	 */
	private $_current_EVR = array();

	/**
	 * Error message thrown by the last method call. Empty string = no error.
	 *
	 * @var	string
	 */
	private $_err_msg = '';

	/*
	 * The object which controls the EVR form being rendered or received.
	 *
	 * @var	object
	 */
	private $_formObj = NULL;

	/*
	 * The object which gives access to all application package properties and methods.
	 *
	 * @var	object
	 */
	private $_appObj;

	// TODO: Add email address validation against duplication with existing Moodle users

	//**********************************************************************************************
	/**
	 * Class constructor. If a form was not submitted, then this class expects to get it from the
	 * 'ApplicationType' VarTag. If a form WAS submitted, then the application value from the form
	 * is used.
	 *
	 * If $flag_bad_type is TRUE, then any application type error will log a class-level error
	 * message. Otherwise, such a condition is left unflagged.
	 *
	 * @param	bool	$flag_bad_type	TRUE = See above
	 */
	function __construct( $flag_bad_type = TRUE ) {

		// Save the application type specified in the VarTag ApplicationType. This VarTag
		// will be updated from a possibly-submitted EVR form
		$VTMgr = new ohflib_VarTagManager();
		$this->_application_type = $VTMgr->getVarTagValue( 'ApplicationType' );
		// Instantiate the application package object. Execution halts with an error if
		// there is no installed application package
		$this->_appObj = amp_newApplicationPkgObj( __FUNCTION__, __LINE__ );
		// Instantiate the form object and get any data that might have been submitted
		$this->_formObj = new amp_EVR_Form();
		$form_data = $this->_formObj->get_data();
		if ( ! is_null( $form_data ) ) {
			// There was submitted EVR data, so make it the current record
			unset( $form_data->submitbutton ); // Don't want this form-engine debris
			$this->_current_EVR = (array)$form_data;
			if ( $this->isNew() ) {
				// The submission is a new EVR, so set the time stamps
				$this->_current_EVR['time_requested'] = time();
				$this->_current_EVR['time_validated'] = 0;
			}
			// Get the application type from the submitted record
			$this->_application_type = $this->_current_EVR['application_type'];
		}

		// Create VarTag definitions which are used by the plugin settings page or other
		// callers which do not require the application type context
		$this->updateAppTypeVarTags( $flag_bad_type );
		return;

	} // end of constructor


	//**********************************************************************************************
	/**
	 * Makes the specified EVR the current one. If $id is NULL, then then any current EVR will be
	 * discarded and there will be no current EVR. If $id is invalid (there is no table entry with
	 * the specified ID), there will be no current EVR.
	 *
	 * @param	int		$id		ID of the existing EVR record
	 *
	 * @return	void
	 */
	function setCurrent( $id = NULL ) {

		global $DB;

		$this->_err_msg = '';
		$this->_current_EVR = array();
		if ( ! is_null( $id ) ) {
			// The ID is presumed to be an integer
			$record = $DB->get_record( $this->_EVR_table_name, array( "id" => $id ), "*" );
			if ( $record === FALSE ) {
				$this->_err_msg = "EVR record (ID '$id') does not exist";
			} else {
				$this->_current_EVR = (array)$record;
				$this->_application_type = $this->_current_EVR['application_type'];
			}
			$this->updateAppTypeVarTags();
		}
		return;

	} // end of method setCurrent()


	//**********************************************************************************************
	/**
	 * Updates the 'ApplicationType' VarTag with the application type value from the current
	 * EVR. If there is no current EVR or the current application type value is invalid, the VarTags
	 * are created with empty values.
	 *
	 * When $flag_bad_type is TRUE, then any application type error will log a class-level error
	 * message. Otherwise, such a condition is left unflagged.
	 *
	 * @param	bool	$flag_bad_type	TRUE = See above
	 *
	 * @return	void
	 */
	function updateAppTypeVarTags( $flag_bad_type = TRUE ) {

		global $CFG;

		$this->_err_msg = '';
		$app_type = $this->_application_type;
		$VT_MgrObj = new ohflib_VarTagManager();
		$groups = array( 'application' );

		$desc = 'Identifies the application type (ie., "student")';
		$VT_MgrObj->setVarTagValue( 'ApplicationType', $app_type, $desc, $groups );

		$value = $this->_appObj->getApplicationTitle( $app_type );
		$desc = 'The title of the application in focus';
		$VT_MgrObj->setVarTagValue( 'ApplicationTitle', $value, $desc, $groups );

		$value = $this->_appObj->getApplicationDescription( $app_type );
		$desc = 'The description of the application in focus';
		$VT_MgrObj->setVarTagValue( 'ApplicationDescription', $value, $desc, $groups );

		$evrid = $this->getCurrentId();
		$value = $CFG->wwwroot . $this->_appObj->getApplicationHandler( $app_type )
			. "?evrid=$evrid";
		$desc = 'URL of the online application';
		$VT_MgrObj->setVarTagValue( 'ApplicationURL', $value, $desc, $groups );

		// Bubble up to this class any error caused by an invalid application type
		if ( $flag_bad_type ) {
			$this->_err_msg = $this->_appObj->getClassErrorMsg();
		}
		return;

	} // end of method updateAppTypeVarTags()


	//**********************************************************************************************
	/**
	 * Returns the applicant name of the current EVR.
	 *
	 * @return	string
	 */
	function getApplicantName() {

		$result = $this->_current_EVR['first_name'] . " " . $this->_current_EVR['last_name'];
		return $result;

	} // end of method getApplicantName()


	//**********************************************************************************************
	/**
	 * Returns the record ID of the current EVR. If there is no current EVR, then -1 is returned.
	 *
	 * @return	string
	 */
	function getCurrentId() {

		if ( isset( $this->_current_EVR['id'] ) ) {
			$result = $this->_current_EVR['id'];
		} else {
			$result = -1;
		}
		return $result;

	} // end of method getCurrentId()


	//**********************************************************************************************
	/**
	 * Returns the application type of the current EVR. If there is no current EVR, then an empty
	 * string is returned.
	 *
	 * @return	string
	 */
	function getCurrentAppType() {

		if ( isset( $this->_current_EVR['application_type'] ) ) {
			$result = $this->_current_EVR['application_type'];
		} else {
			$result = '';
		}
		return $result;

	} // end of method getCurrentAppType()


	//**********************************************************************************************
	/**
	 * Renders the HTML display of all EVRs. The caller must at least instantiate $JDP_Obj because
	 * this method will add data to it. JDP data will be required by the JavaScript function
	 * confirmDeletion() if the user clicks the DELETE icon rendered by this method.
	 *
	 * By default, the table is rendered in descending order by time_requested. Different sorting
	 * can be achieved by setting $sort_order to the content of an SQL ORDER BY clause (see
	 * http://www.w3schools.com/sql/sql_orderby.asp).
	 *
	 * @param	string	$sort_order	Sort specification as defined via the SQL ORDER BY clause
	 *
	 * @return	string				HTML EVR table display
	 */
	function renderEVRDisplay( $sort_order = "time_requested desc" ) {

		global $DB;

		// Display the current validations
		$tblObj = new ohflib_quicktable( "app_info_display" );
		$tblObj->defineColumn( "last_name", "Last Name" );
		$tblObj->defineColumn( "first_name", "First Name" );
		$tblObj->defineColumn( "country_code", "Country" );
		$tblObj->defineColumn( "email_address", "Email Address" );
		$tblObj->defineColumn( "time_requested", "Time Requested" );
		$tblObj->defineColumn( "time_validated", "Time Validated" );
		$tblObj->defineColumn( "application_type", "App Type" );
		$tblObj->defineColumn( "application_id", "App ID" );
		$tblObj->defineColumn( "actions", "Actions" );
		$records = $DB->get_records( $this->_EVR_table_name, array(), $sort_order );
		$date_format = "F j, Y, g:i a";
		$countryObj = new ohflib_countries();
		$plugin_name = 'local_amp';
		// Loop through the defined applications to add table data to the table rendering class
		foreach( $records as $idx => $record ) {
			// Add the record's data to each cell of the table definition
			$tblObj->startRecord();
			$tblObj->setColumnData( "last_name", $record->last_name );
			$tblObj->setColumnData( "first_name", $record->first_name );
			$country_name = $countryObj->getCountryName( $record->country_code );
			$tblObj->setColumnData( "country_code", $country_name );
			$tblObj->setColumnData( "email_address", $record->email_address );
			$tblObj->setColumnData( "time_requested", date( $date_format, (int)$record->time_requested ) );
			$tblObj->setColumnData( "time_validated", ( $record->time_validated > 0 ) ? date( $date_format, (int)$record->time_validated ) : "" );
			$tblObj->setColumnData( "application_type", $record->application_type );
			$tblObj->setColumnData( "application_id", ( $record->application_id < 0 ) ? '' : (int)$record->application_id );
			// Build the action icons for this record...
			$id = $record->id;
			$paramObj = new stdClass();
			$paramObj->name = $record->first_name . " " . $record->last_name;
			// ...record deletion
			// This code was neutered to allow EVRs to be deleted even if they are
			// linked to a completed application (because the deletion of EVRs via
			// user-record deletion involves Moodle event hooking, something that is
			// too complex for consideration at the moment) - kaw
			// TODO: Implement EVR-record-deletion linkage to user-record deletion and re-enable this code - kaw
			//if ( $record->application_id >= 0 ) {
				// There is a linked application, so do not allow deletion
				//$tooltip_id = "tooltipdeleteblock";
				//$function = "delete_symbol";
			//} else {
				$tooltip_id = "tooltipdelete";
				$function = "delete";
			//}
			$deletion_confirmation_msg = get_string( 'confirmdelprompt', $plugin_name, $paramObj );
			$tooltip = get_string( $tooltip_id, $plugin_name, $paramObj );
			$actions = $this->makeActionElement( "delete_evr$id", $function, $tooltip, $record->id, $deletion_confirmation_msg ) . "&nbsp;&nbsp;";
			// Make record editing table action element
			$tooltip = get_string( "tooltipedit", $plugin_name, $paramObj );
			$actions .= $this->makeActionElement( "edit_evr$id", "edit", $tooltip, $record->id ) . "&nbsp;&nbsp;";
			// Make email resending table action element
			$tooltip = get_string( "tooltipresend", $plugin_name, $paramObj );
			$actions .= $this->makeActionElement( "resend_evr$id", "resend", $tooltip, $record->id );
			$tblObj->setColumnData( "actions", $actions );
			$tblObj->endRecord();
		}
		// Set the JDP data, render the table and quit
		echo $tblObj->renderTableHTML();
		return;

	} // end of method renderEVRDisplay()


	//**********************************************************************************************
	/**
	 * Renders a single action link/icon for the email validation request display table. If the
	 * variable $function is 'delete', then this method expects that JavaScript will be loaded with
	 * a confirmDeletion() function whose purpose is to provide a deletion-confirmation dialog.
	 *
	 * @param	string	$element_id	Value of the link element's ID property
	 * @param	string	$function	edit | resend | delete
	 * @param	string	$title		The tooltip-style title to display when hovering over the icon
	 * @param	int		$record_id	Indicates the email validation request record in focus
	 * @param	string	$delconfmsg	Deletion confirmation string (used if $function = 'delete')
	 * @param	string	$class		The class property value, if provided
	 *
	 * @return	string				Rendered HTML
	 */
	private function makeActionElement( $element_id, $function, $title, $record_id, $delconfmsg = '', $class = '' ) {

		if ( $class <> '' ) {
			// A class has been provided, so format it to have a single prefixed space character
			$class = " class='" . trim( $class ) . "'";
		}
		if ( $function == "delete" ) {
			// confirmDeletion() will transfer to the specified URL (with the deletion parameters) upon user confirmation
			$link_properties = "href='#' onclick='confirmDeletion(\"$delconfmsg\",\"evrmanagement.php?a=$function&id=$record_id\")'";
		} elseif ( $function == "delete_symbol" ) {
			// Just render the delete symbol only with its tooltip
			$function = "delete";
			$link_properties = "";
		} else {
			// No JS invocation, so just invoke the specified operation
			$link_properties = "href='evrmanagement.php?a=$function&id=$record_id'";
		}
		$icon_url = ohflib_getCurrentURLHost() . "/local/amp/images/$function.svg";
		$result = "<a id='$element_id' $link_properties title='$title'$class><img src='$icon_url'></a>";
		return $result;

	} // end of function makeActionElement()


	//**********************************************************************************************
	/**
	 * Renders the form to create or edit an EVR. If there is no current EVR, the form's controls
	 * will be empty or set to defaults. If there is an EVR, the form will be preloaded with that
	 * EVR's data. Be aware that if a form submission is pending, those submitted values will take
	 * precedence over the values of a current EVR.
	 *
	 * @return	string		Rendered HTML
	 */
	function renderEVRForm() {

		if ( $this->isLoaded() ) {
			// There is a current EVR, so copy the values from the current EVR into the form
			$data_to_preload['id'] = $this->_current_EVR['id'];
			$data_to_preload['first_name'] = $this->_current_EVR['first_name'];
			$data_to_preload['last_name'] = $this->_current_EVR['last_name'];
			$data_to_preload['country_code'] = $this->_current_EVR['country_code'];
			$data_to_preload['email_address'] = $this->_current_EVR['email_address'];
			$data_to_preload['time_requested'] = $this->_current_EVR['time_requested'];
			$data_to_preload['time_validated'] = $this->_current_EVR['time_validated'];
			$data_to_preload['application_type'] = $this->_current_EVR['application_type'];
			$this->_formObj->clearFormData();
			$this->_formObj->set_data( $data_to_preload );
		}
		$html = $this->_formObj->render();
		return $html;

	} // end of method renderEVRForm()


	//**********************************************************************************************
	/**
	 * Returns the data of the current EVR. If there is no current EVR, an empty array
	 * will be returned.
	 *
	 * @return	array
	 */
	function getCurrent() {

		return $this->_current_EVR;

	} // end of method getCurrent()


	//**********************************************************************************************
	/**
	 * Saves the current EVR, if it exists, to the EVR table. No catchable error is logged.
	 *
	 * @return	void
	 */
	function saveCurrent() {

		global $DB;

		if ( $this->isLoaded() ) {
			// A valid EVR exists (either new or edited), so save it
			if ( $this->_current_EVR['id'] < 0 ) {
				// This is a new EVR
				unset( $this->_current_EVR['id'] );
				$recObj = (object)$this->_current_EVR;
				$this->_current_EVR['id'] = $DB->insert_record( $this->_EVR_table_name, $recObj, TRUE );
			} else {
				// This is an edited EVR
				$recObj = (object)$this->_current_EVR;
				$DB->update_record( $this->_EVR_table_name, $recObj );
			}
		}
		return;

	} // end of method saveCurrent()


	//**********************************************************************************************
	/**
	 * Deletes the current EVR. An error is logged if there is no current EVR.
	 *
	 * @return	void
	 */
	function deleteCurrent() {

		global $DB;

		if ( $this->isLoaded() ) {
			$DB->delete_records( $this->_EVR_table_name, array( "id" => $this->_current_EVR['id'] ) );
			$this->_current_EVR = array();
		} else {
			$this->_err_msg = "There is no current record.";
		}
		return;

	} // end of method delete()


	//**********************************************************************************************
	/**
	 * Returns TRUE if there is a currently-loaded EVR.
	 *
	 * @return	bool
	 */
	function isLoaded() {

		return ( count( $this->_current_EVR ) > 0 );

	} // end of method isLoaded()


	//**********************************************************************************************
	/**
	 * Returns TRUE if the currently-loaded EVR is newly-submitted (has a record ID < 0).
	 *
	 * @return	bool
	 */
	function isNew() {

		// The ID value provided by a new-record form is always -1
		return ( $this->_current_EVR['id'] < 0 );

	} // end of method isNew()


	//**********************************************************************************************
	/**
	 * Returns TRUE if the currently-loaded EVR is both new and a duplicate of an existing EVR
	 * record. Comparison is made only against fields which can be edited in the EVR form. Duplicate
	 * EVRs can easily be submitted by refreshing the page containing the EVR form, and this
	 * method enables the caller to process it knowingly.
	 *
	 * @return	bool
	 */
	function isDuplicate() {

		global $DB;

		// Remove the ID and date values from a copy of the current EVR, and test the
		// remaining values against the EVR table
		$params = $this->_current_EVR;
		unset( $params['id'] );
		unset( $params['date_requested'] );
		unset( $params['date_validated'] );
		unset( $params['application_id'] );
		$num_recs = $DB->count_records( $this->_EVR_table_name, $params );
		return ( $num_recs > 0 );

	} // end of method isDuplicate()


	//**********************************************************************************************
	/**
	 * Returns TRUE if the data values in the currently-loaded EVR have been validated (contain no
	 * omissions or structural errors). This function tests the values in the form, not the current
	 * EVR, but they should be the same.
	 *
	 * @return	bool
	 */
	function isValidated() {

		return ( $this->_formObj->is_validated() );

	} // end of method isValidated()


	//**********************************************************************************************
	/**
	 * Returns TRUE if an error occurred during the previous class method.
	 *
	 * @return	bool
	 */
	function errorOccurred() {

		return ( $this->_err_msg <> '' );

	} // end of method errorOccurred()


	//**********************************************************************************************
	/**
	 * Returns TRUE if an error occurred during the previous class method.
	 *
	 * @return	string
	 */
	function getClassErrorMsg() {

		return $this->_err_msg;

	} // end of method getClassErrorMsg()


	//**********************************************************************************************
	/**
	 * Returns an HTML rendering of validation error messages. Multiple errors returns a formatted
	 * unordered list.
	 *
	 * @return	string
	 */
	function getFormErrorMsgs() {

		return $this->_formObj->renderValidationErrors();

	} // end of method getFormErrorMsgs()


	//**********************************************************************************************
	/**
	 * Updates the application type and validation time of the current EVR record. Optionally, the
	 * changes to the values can be made without committing the whole record to the database.
	 *
	 * The application type is a simple string, typically the display name of an application taken
	 * from the application package plugin.
	 *
	 * The validation time will be 0 or the specified Unix timestamp value. $time can be any non-
	 * negative value. A value greater than 0 indicates that the EVR has been validated. If no time
	 * value is provided, the current server time is used. Setting the type to empty string and the
	 * time to 0 resets an existing validation.
	 *
	 * Nothing happens if there is no current EVR.
	 *
	 * @param	string	$application_type	Type of application
	 * @param	int		$time				A Unix timestamp
	 * @param	bool	$OK_to_save_record	TRUE = Save the record after updating the fields
	 *
	 * @return	void
	 */
	function markAsValidated( $application_type, $time = -1, $OK_to_save_record = TRUE ) {

		if ( $this->isLoaded() ) {
			// Update application as being reviewed
			$this->_current_EVR['application_type'] = $application_type;
			$this->_current_EVR['time_validated'] = ( $time < 0 ) ? time() : $time;
			if ( $OK_to_save_record ) {
				$this->saveCurrent();
			}
		}
		return;

	} // end of method markAsValidated()


	//**********************************************************************************************
	/**
	 * Sets the application ID of the current EVR record to the specified value. Nothing happens if
	 * there is no current EVR.
	 *
	 * @param	int		$application_id		ID of an application record
	 * @param	bool	$OK_to_save_record	TRUE = Save the record after updating the time
	 *
	 * @return	void
	 */
	function updateApplicationId( $application_id, $OK_to_save_record = TRUE ) {

		if ( $this->isLoaded() ) {
			// Update application as being reviewed
			$this->_current_EVR['application_id'] = $application_id;
			if ( $OK_to_save_record ) {
				$this->saveCurrent();
			}
		}
		return;

	} // end of method updateApplicationId()


	//**********************************************************************************************
	/**
	 * Sends the EVR acknowledgement email to the email address in the current EVR. Nothing happens
	 * if there is no current EVR.
	 *
	 * @param	string	$message_body	The message to be sent
	 * @param	bool	$ok_to_debug	TRUE = return rendered debugging snapshot of email object
	 *
	 * @return	mixed					void unless $ok_to_debug is TRUE
	 */
	function sendValidationEmail( $message_body, $ok_to_debug = FALSE ) {

		global $CFG;

		if ( $this->isLoaded() ) {
			// An EVR record is current, so preset some value-only VarTags
			$VT_Mgr = new ohflib_VarTagManager();
			$snapshot = '';
			//if ( $ok_to_debug ) {
			//	$snapshot = $VT_Mgr->dumpDefinitions();
			//}
			$this->updateAppTypeVarTags();
			$VT_Mgr->setVarTagValue( 'ApplicantName', $this->getApplicantName() );
			$VT_Mgr->setVarTagValue( 'EVR_ID', $this->getCurrentId(), 'Email validation record ID', array( 'system' ) );
			// Set the message properties
			$emailObj = new ohflib_EmailManager();
			$emailObj->isSendmail();
			$emailObj->setWordWrap();
			$emailObj->setMessageFormat( 'plain' );
			$emailObj->addReplyTo( $CFG->amp_validationemailaddr );
			$emailObj->setFrom( $CFG->amp_validationemailaddr );
			$emailObj->addAddress( $this->_current_EVR['email_address'] );
			$bcc_addresses = explode( "\n", $CFG->amp_validationemailbccaddr );
			// Extract, trim and set any defined BCC addresses
			foreach( $bcc_addresses as $address ) {
				// Since only "\n" is the delimiter because it always appears in all line-
				// ending scenarios, "\r" might still have to be trimmed. Surrounding spaces
				// entered in the BCC setting must also be discarded
				$address = trim( $address );
				if ( $address <> '' ) {
					// After trimming, this line still has address content, so add it
					$emailObj->addBCC( $address );
				}
			}
			//$emailObj->addAddress("ken.w@100fold.it");
			$emailObj->setSubject( $CFG->amp_validationemailsubj );
			$emailObj->setBody( $message_body );
			//$emailObj->AltBody = $message_body;
			$emailObj->transmit();
			if ( $ok_to_debug ) {
				$snapshot .= $emailObj->getDebugSnapshot( '$emailObj' );
				return $snapshot;
			}
		}
		return;

	} // end of method sendValidationEmail()


	//**********************************************************************************************
	/**
	 * Dumps for debugging any submitted form data.
	 *
	 * @param	bool	$OK_to_echo	TRUE = echo to the output
	 *
	 * @return	string
	 */
	function dumpFormData( $OK_to_echo = FALSE ) {

		if ( isset( $this->_formObj->_submitValues ) ) {
			$form_data = $this->_formObj->_submitValues;
			$form_data_array = (array)$form_data;
		} else {
			$form_data_array = array();
		}
		$html = ohflib_dumpVar( '$form_data_array', $form_data_array, $OK_to_echo );
		return $html;

	} // end of method dumpFormData()

} // end of class amp_EVR_Manager