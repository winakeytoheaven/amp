<?php

/**
 * This class describes an empty application package which is used if no application package
 * is selected in the AMP settings.
 *
 * This class and all sister classes (for other organizations/programs) will be included by the
 * AMP plugin via a reference to <plugin_path>/classes/class.applications.php and instantiated by
 * $<variable> = new <plugin_name>_applications().
 *
 * @package   localamp
 * @copyright 2016 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class EmptyApplicationPackage {

	/**
	 * Plugin name ([type]_[name])
	 *
	 * @var	string	$_plugin_name
	 */
	private $_plugin_name = '';

	/**
	 * Plugin path ([type]/[name])
	 *
	 * @var	string	$_plugin_path
	 */
	private $_plugin_path = '';

	/**
	 * Title of the organization to which the applications defined here is related
	 *
	 * @var	string $_org_title
	 */
	private $_org_title = '[undefined-organization]';

	/**
	 * Street address of the organization
	 *
	 * @var	string $_street
	 */
	private $_street = '[undefined-address]';

	/**
	 * City name of the organization
	 *
	 * @var	string $_city
	 */
	private $_city = '[undefined-city]';

	/**
	 * State/Province name of the organization
	 *
	 * @var	string $_state
	 */
	private $_state = 'XX';

	/**
	 * Postal code of the organization
	 *
	 * @var	string $_postal_code
	 */
	private $_postal_code = '99999';

	/**
	 * Title of the application package to which the applications defined here is related
	 *
	 * @var	string $_pkg_title
	 */
	private $_pkg_title = '[undefined-academic-program]';

	/**
	 * Set by the constructor, this array holds each application definition. A definition contains
	 * the application's ID, title, handlers (PHP modules which implement each of the different
	 * operations related to that application, pages (settings-defined content related to the
	 * application) and reference forms (each having its own handlers).
	 *
	 * Each element of this array has this structure:
	 *		[app_type_id] => array(
	 *			'title' =>				[string-id] (used to lookup the application title),
	 *			'desc' =>				[string-id] (used to lookup the application description),
	 *			'handler' =>			Performs all application-form rendering and editing ([path/][PHP_module.php]),
	 *			'refhandler' =>			Performs all reference-form rendering and editing ([path/][PHP_module.php]),
	 *			'refsreqd' =>			[number-of-required-references],
	 *			'render_form_to' =>		[array] List of roles whose menu gets a link to this application
	 *			'render_appmgt_to' =>	[array] List of roles whose menu gets a link to manage this application
	 *			'render_evrmgt_to' =>	[array] List of roles whose menu gets a link to manage EVRs for this application
	 *			'pages' => array(
	 *				[page_title1] => array(
	 *					'title'	=>		[string-id] (used to lookup the correct language string),
	 *					'content' =>	plugin setting which holds the content template ([plugin-root-name]_[setting-name])
	 *					'render_to' =>	[array] List of roles whose menu gets a link to view this page
	 *				),
	 *				...
	 *				[page_titleX] => array(
	 *					'title'	=>		[string-id] (used to lookup the correct language string),
	 *					'content' =>	plugin setting which holds the content template ([plugin-root-name]_[setting-name])
	 *					'render_to' =>	[array] List of roles whose menu gets a link to view this page
	 *				)
	 *			)
	 *		)
	 *
	 * @var	array	$_app_defns
	 */
	private $_app_defns;

	/**
	 * Defines the roles permitted to invoke the AMP application-management module
	 *
	 * @var	array	$_app_mgr_roles
	 */
	private $_app_mgr_roles;

	/**
	 * Defines the roles permitted to invoke the AMP Email Validation management module
	 *
	 * @var	array	$_evr_mgr_roles
	 */
	private $_evr_mgr_roles;

	/**
	 * Built by the constructor, this array hold organization-level definitions of top-level handlers
	 * for the various functions required by this application package.
	 *
	 * Each element of this array has this structure:
	 *		[function_type_id] => array(
	 *			'link_text' =>	[string_id],
	 *			'handler' =>	Module which handles this function ([path/][PHP_module.php])
	 *		)
	 *
	 * @var	array	$_pkg_handler_defns
	 */
	private $_pkg_handler_defns;

	/**
	 * Controls the rendering of values which may potentially include colored elements. TRUE
	 * indicates that the colored elements should be included in the rendering. See the method
	 * renderTriColorValue().
	 */
	private $_include_colored_elements = TRUE;

	/**
	 * An instance of the Role Manager is created for use by this class to help reduce
	 * the work of re-instantiation.
	 *
	 * @var	object
	 */
	private $_role_Mgr;

	/**
	 * Error message thrown by the last method call. Empty string = no error.
	 *
	 * @var	string
	 */
	private $_err_msg = '';

	//**********************************************************************************************
	/**
	 * This is the class constructor.
	 */
	function __construct() {

		// Set up the application definitions
		$this->_app_defns = array();

		// Set up roles for accessing AMP management modules
		$this->_app_mgr_roles = array( 'Manager', 'Non-editing Teacher' );
		$this->_evr_mgr_roles = array( 'Manager' );

		// Set up package handler definitions
		$this->_pkg_handler_defns = array();

		// Create application-specific VarTags
		$VT_MgrObj = new ohflib_VarTagManager();

		// OrganizationName
		// PackageTitle
		$VT_MgrObj->createDefinition(
			$this->_plugin_name,
			'PackageTitle',
			'Title of this application package (eg., the title of the curriculum, program or degree)',
			array(),
			array( 'package' ),
			$this->_pkg_title
		);
		$VT_MgrObj->createDefinition(
			$this->_plugin_name,
			'OrgName',
			'Name of the organization offering the defined application(s)',
			array(),
			array( 'package' ),
			$this->_org_title
		);
		// PostalCode
		$VT_MgrObj->createDefinition(
			$this->_plugin_name,
			'OrgStreetAddress',
			'Street address of the organization',
			array(),
			array( 'package' ),
			$this->_street
		);
		// OrgCity
		$VT_MgrObj->createDefinition(
			$this->_plugin_name,
			'OrgCity',
			'City name of the organization',
			array(),
			array( 'package' ),
			$this->_city
		);
		// OrgStateProvince
		$VT_MgrObj->createDefinition(
			$this->_plugin_name,
			'OrgStateProvince',
			'State/province of the organization',
			array(),
			array( 'package' ),
			$this->_state
		);
		// OrgPostalCode
		$VT_MgrObj->createDefinition(
			$this->_plugin_name,
			'OrgPostalCode',
			'Postal code of the organization',
			array(),
			array( 'package' ),
			$this->_postal_code
		);

		return;

	} // end of constructor


	//**********************************************************************************************
	/**
	 * Returns this plugin's package name.
	 *
	 * @return	string
	 */
	function getPackageName() {

		return $this->_pkg_title;

	} // end of method getPackageName()


	//**********************************************************************************************
	/**
	 * Returns the relative path to the top-level file which implements the specified operation.
	 *
	 * @param	string	$operation_type_id	Identifies the operation
	 *
	 * @return	string						Relative path to the handler module
	 */
	function getOperationHandler( $operation_type_id ) {

		return "bad_operation_handler.php?functiontypeid=$operation_type_id";

	} // end of method getOperationHandler()


	//**********************************************************************************************
	/**
	 * Returns the link text for the specified operation.
	 *
	 * @param	string	$operation_id	Identifies the operation
	 *
	 * @return	string					Operation description (link text)
	 */
	function getOperationHandlerLinkText( $operation_id ) {

		if ( isset( $this->_pkg_handler_defns[$operation_id]['link_text'] ) ) {
			$link_text = $this->_pkg_handler_defns[$operation_id]['link_text'];
			$result = get_string( $link_text, $this->_plugin_name );
		} else {
			$result = ohflib_wrapWithSpan( "Bad BTSCCM operation ID '$operation_id'" );
		}
		return $result;

	} // end of method getOperationHandlerLinkText()


	//**********************************************************************************************
	/**
	 * Returns this plugin's organization name.
	 *
	 * @return	string
	 */
	function getOrganizationName() {

		return $this->_org_title;

	} // end of method getOrganizationName()


	//**********************************************************************************************
	/**
	 * Returns the title of the specified application type ID.
	 *
	 * @param	string	$application_type	Indicates the desired application
	 *
	 * @return	string						Application type title
	 */
	function getApplicationTitle( $application_type ) {

		return ohflib_wrapWithSpan( "Undefined application title" );

	} // end of method getApplicationTitle()


	//**********************************************************************************************
	/**
	 * Returns the description of the specified application type ID, and sets a class-level error
	 * if $application_type_id is not valid.
	 *
	 * @param	string $application_type 	Indicates the desired application
	 *
	 * @return	string						Relative path of top-level module
	 */
	function getApplicationDescription( $application_type ) {

		return ohflib_wrapWithSpan( "Undefined application description" );

	} // end of method getApplicationDescription()


	//**********************************************************************************************
	/**
	 * Returns the relative path of top-level module designated as the handler for the specified
	 * application type ID.
	 *
	 * @param	string	$application_type	Indicates the desired application
	 *
	 * @return	string						Relative path of top-level module
	 */
	function getApplicationHandler( $application_type ) {

		return "undefined_app_handler.php";

	} // end of method getApplicationHandler()


	//**********************************************************************************************
	/**
	 * Returns the number of defined application forms as either an integer or string.
	 *
	 * @param	string		'integer' or 'string
	 *
	 * @return	mixed
	 */
	function getNumApplications( $mode = 'string' ) {

		$result = 0;
		if ( $mode == 'string' ) {
			$result = (string)$result;
		}
		return $result;

	} // end of method getNumApplications()


	//**********************************************************************************************
	/**
	 * Returns a simple array of the defined application ID values
	 *
	 * @return	array
	 */
	function getApplicationIdList() {

		return array();

	} // end of method getApplicationIdList()


	//**********************************************************************************************
	/**
	 * Returns a simple array of the defined page ID values for the specified Application ID.
	 * An empty array is returns if the specified $application_id is invalid.
	 *
	 * @param	string	$application_id	The application ID whose page ID values are wanted
	 *
	 * @return	array
	 */
	function getPageIdList( $application_id ) {

		return array();

	} // end of method getPageIdList()


	//**********************************************************************************************
	/**
	 * Renders the list of applications as a single link or an unordered list of links (depending
	 * on the number of existing defined applications). This function presumes that there is at
	 * least 1 defined application in this class. It also presumes that the VarTag @@EVR_ID@@ was
	 * defined (to point to a valid EVR record) prior to invocation. If @@EVR_ID@@ was not defined,
	 * the ID used will be 0. The provided URL will carry parameters which specify both the
	 * application ID and the Email Validation Request ID.
	 *
	 * NOTE: The 'HTML' format is not being maintained - kaw
	 *
	 * @param	string	$format				'plain' = plain text, 'html' = HTML
	 * @param	bool	$ok_to_show_pages	TRUE = include associated pages in display
	 *
	 * @return	string						Rendered HTML
	 */
	function renderApplicationList( $format = 'plain', $ok_to_show_pages = FALSE ) {

		return '';

	} // end of method renderApplicationList()


	//**********************************************************************************************
	/**
	 * Renders the application information in an HTML table, often used in a plugin settings page.
	 *
	 * @return	string	Rendered HTML
	 */
	function renderAppInfoDisplay() {

		$tblObj = new ohflib_quicktable( "app_info_display" );
		$tblObj->defineColumn( "app_name", "Application" );
		$tblObj->defineColumn( "app_desc", "Description" );
		$tblObj->defineColumn( "reference", "# Refs" );
		$tblObj->defineColumn( "relateddocs", "Related Page(s)" );
		$result = $tblObj->renderTableHTML();
		return $result;

	} // end of method renderAppInfoDisplay()


	//**********************************************************************************************
	/**
	 * Renders a string from either 1 or 3 parts in 1 or 3 colors (default, green and blue), depending
	 * on the value of the class property include_colored_elements. If it is FALSE, the $default_part
	 * alone is returned unchanged. If it is TRUE, the returned string will be of the form
	 * "default_part [green_part, blue_part]". If green_part or blue_part are empty, then those parts
	 * do not appear in the rendered string
	 *
	 * @param	string	$default_part
	 * @param	string	$green_part
	 * @param	string	$blue_part
	 *
	 * @return	string
	 */
	function renderTriColorValue( $default_part, $green_part, $blue_part ) {

		$result = $default_part;
		$colored_part = '';
		if ( $this->_include_colored_elements ) {
			$colored_part .= ( $green_part == '' ) ? '' : ohflib_wrapWithSpan( $green_part, 'green' );
			if ( $colored_part != '' and $blue_part != '' ) {
				$colored_part .= ', ';
			}
			$colored_part .= ( $blue_part == '' ) ? '' : ohflib_wrapWithSpan( $blue_part, 'blue' );
			$result .= ' ' . ohflib_encloseString( $colored_part, '[' );
		}
		return $result;

	} // end of method renderTriColorValue()


	//**********************************************************************************************
	/**
	 * Renders a tabular display of the VarTags which relate to the applications defined in this
	 * class.
	 *
	 * @return	string
	 */
	function renderVarTagDisplay() {

		$vtmgrObj = new ohflib_VarTagManager();
		$result = $vtmgrObj->renderDisplay();
		return $result;

	} // end of method renderVarTagDisplay()


	//**********************************************************************************************
	/**
	 * Returns the URL for the settings page for the plugin which houses this class.
	 *
	 * @return	string
	 */
	function makeSettingsPageURL() {

		return "bad_settings.php";

	} // end of method makeSettingsPageURL()


	//**********************************************************************************************
	/**
	 * Returns an array of control information (link text and URL) for assembling the custom
	 * Navigation menu elements which correspond to the roles possessed by the current user. It is
	 * the caller's responsibility to build the menu.
	 *
	 * The parameters must be supplied by the caller (presumably the AMP plugin) because
	 *		1.	the Email Validation Request (EVR) mechanism might be invoked when the user clicks
	 *			a link to show the desired application form, and
	 *		2.	the application-rendering and application-management links must be given URL
	 *			parameters that only the application package can know.
	 *
	 * The format of the control array is
	 *
	 *		array(
	 *			'apps' = array(
	 *				[app-id-1] => array(
	 *					'form' => array( 'text' => [link-text], 'url' => [moodle_url] )
	 *					'pages' => array(
	 *						[page-id-1] => array( 'text' => [link-text], 'url' => [moodle_url] ),
	 *						...
	 *						[page-id-N] => array( 'text' => [link-text], 'url' => [moodle_url] )
	 *					),
	 *				),
	 *				...
	 *				[app-id-N] => array(
	 *					same as app-id-1 above
	 *				),
	 *			),
	 *			'appmgt' = array( 'text' => [link-text], 'url' => [moodle_url] ),
	 *			'evrmgt_allowed' = [boolean] (TRUE = user can manage EVRs if EV is enabled)
	 *		)
	 *
	 * Be aware that the returned control array's 'form' and/or 'pages' elements for a particular
	 * application may not exist, depending on the mix of enabling roles and the current user's
	 * role(s). Also be aware that URL parameters will be added to
	 *
	 * @param	string	$app_form_url	Base URL of the module to handle form creation/processing
	 * @param	string	$assoc_page_url	Base URL of the module to handle associated page rendering
	 *
	 * @return	array
	 */
	function getNavigationMenuInfo( $app_form_url, $assoc_page_url ) {

		$result = array();
		return $result;

	} // end of method getNavigationMenuInfo()


	//**********************************************************************************************
	/**
	 * Returns the top-level PHP file created to handle operations involving the specified
	 * application type.
	 *
	 * @param	string	$application_type	Specifies the application definition
	 *
	 * @return	string						File name (in the form '[plugin_name]/[file].php')
	 */
	function getFormHandler( $application_type ) {

		$result = '';
		return $result;

	} // end of method getFormHandler()


	//**********************************************************************************************
	/**
	 * Returns the HTML for the specified application, potentially pre-loaded with the specified
	 * applicant's data. Any parameter error renders an appropriate message.
	 *
	 * @param	string	$application_type_id	Indicates the desired application
	 * @param	integer	$applicant_id			-1 (render empty form) or preload with applicant's data
	 *
	 * @return	string
	 */
	function renderApplicationForm( $application_type_id, $applicant_id = -1 ) {

		return '';

	} // end of method renderApplicationForm()


	//**********************************************************************************************
	/**
	 * Returns the HTML for the specified application's Reference form preloaded with some of the
	 * specified applicant's data. Any parameter error renders an appropriate message.
	 *
	 * @param	string	$application_type_id	Indicates the desired application
	 * @param	integer	$applicant_id			Indicates the applicant for whom the reference is needed
	 * @param	integer	$referent_id			Indicates the referent for whom the form is intended
	 *
	 * @return	string
	 */
	function renderReferenceForm( $application_type_id, $applicant_id, $referent_id ) {

		return '';

	} // end of method renderReferenceForm()


	//**********************************************************************************************
	/**
	 * Returns the HTML for the specified application's specified auxiliary page. Any parameter
	 * error renders an appropriate message.
	 *
	 * @param	string	$application_type_id	Indicates the desired application
	 * @param	string	$page_id				Indicates the application's desired page
	 *
	 * @return	string
	 */
	function renderPage( $application_type_id, $page_id ) {

		return '';

	} // end of method renderPage()


	//**********************************************************************************************
	/**
	 * Returns the page title of the specified application page.
	 *
	 * @param	string	$application_type_id	Indicates the desired application
	 * @param	string	$page_id				Indicates the application's desired page
	 *
	 * @return	string
	 */
	function getPageTitle( $application_type_id, $page_id ) {

		return 'Undefined page title';

	} // end of method renderPage()


	//**********************************************************************************************
	/**
	 * Validates the specified application type ID and the page ID, returning a message in case of
	 * error or empty string if they are valid.
	 *
	 * @param	string	$application_type_id
	 * @param	string	$page_id
	 *
	 * @return	string
	 */
	function _applicationParamErrMessage(  $application_type_id, $page_id  ) {

		$html = $this->applicationIdErrMessage( $application_type_id );
		if ( $html == '' ) {
			// No type ID error, so test the page ID
			$html = $this->applicationPageIdErrMessage( $application_type_id, $page_id );
		}
		return $html;

	} // end of method _applicationParamErrMessage()


	//**********************************************************************************************
	/**
	 * Returns an empty string if the specified application ID is valid. Otherwise, an error message
	 * is returned.
	 *
	 * @param	string	$application_type_id	Indicates the desired application
	 *
	 * @return	string
	 */
	private function applicationIdErrMessage( $application_type_id ) {

		return "Invalid application type '$application_type_id'";

	} // end of method applicationIdIsValid()


	//**********************************************************************************************
	/**
	 * Presuming that $application_id has already been validated, this function returns an empty
	 * string if the specified page ID is valid. Otherwise, an error message is returned.
	 *
	 * @param	string	$application_type_id	Indicates the desired application
	 * @param	string	$page_id				Indicates the application's desired page
	 *
	 * @return	string
	 */
	private function applicationPageIdErrMessage( $application_type_id, $page_id ) {

		return "Invalid application page id '$page_id'";

	} // end of method applicationPageIdErrMessage()


	//**********************************************************************************************
	/**
	 * Instantiates the Role Manager for later use if it hasn't already been done.
	 *
	 * @return	void
	 */
	private function setupRoleManager() {

		return;

	} // end of method setupRoleManager()


	//**********************************************************************************************
	/**
	 * Returns TRUE if an error occurred during the previous class method.
	 *
	 * @return	bool
	 */
	function errorOccurred() {

		return ( $this->_err_msg <> '' );

	} // end of method errorOccurred()


	//**********************************************************************************************
	/**
	 * Returns TRUE if an error occurred during the previous class method.
	 *
	 * @return	bool
	 */
	function getClassErrorMsg() {

		return $this->_err_msg;

	} // end of method getClassErrorMsg()

} // end of class EmptyApplicationPackage