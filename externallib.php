<?php

/**
 * AMP's API interface. It provides nothing more than a 'hello world' response. See
 * https://docs.moodle.org/dev/Adding_a_web_service_to_a_plugin.
 *
 * @package   localamp
 * @copyright 2016 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

global $CFG;

require_once( $CFG->libdir . "/externallib.php" );

class local_amp_external extends external_api {

	//**********************************************************************************************
	/**
	 * Returns description of method parameters
	 *
	 * @return external_function_parameters
	 */
	public static function hello_world_parameters() {

		return new external_function_parameters( array( 'welcomemessage' => new external_value( PARAM_TEXT, 'The welcome message. By default it is "Hello world,"', VALUE_DEFAULT, 'Hello world, ' ) ) );

	} // end of method hello_world_parameters()

	//**********************************************************************************************
	/**
	 * Returns welcome message to API request.
	 *
	 * @param string $welcomemessage The specified welcome message, defaulting to 'Hello world, '
	 *
	 * @return string welcome message
	 */
	public static function hello_world( $welcomemessage = 'Hello world, ' ) {
		global $USER;

		//Parameter validation
		//REQUIRED
		$params = self::validate_parameters( self::hello_world_parameters(), array( 'welcomemessage' => $welcomemessage ) );

		//Context validation
		//OPTIONAL but in most web service it should present
		$context = get_context_instance( CONTEXT_USER, $USER->id );
		self::validate_context( $context );

		//Capability checking
		//OPTIONAL but in most web service it should present
		if ( !has_capability( 'moodle/user:viewdetails', $context ) ) {
			throw new moodle_exception( 'cannotviewprofile' );
		}

		return $params['welcomemessage'] . $USER->firstname;;

	} // end of method hello_world()


	//**********************************************************************************************
	/**
	 * Returns description of method result value
	 *
	 * @return external_description
	 */
	public static function hello_world_returns() {

		return new external_value( PARAM_TEXT, 'The welcome message + user first name' );

	} // end of method hello_world_returns()

} // end of class local_amp_external
