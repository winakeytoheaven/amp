<?php

/**
 * This file defines the settings (and their form-editing parameters) used by
 * the AMP plugin. This code is invoked by Moodle via the menu link
 *
 *		Site administration | Plugins | Local plugins | <pluginname>
 *
 * @package   localamp
 * @copyright 2016 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


//**********************************************************************************************
// OHFLIB initialization - For details see <document_root>/local/ohflib/dev/initialize.php.
//**********************************************************************************************
require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';
$plugininfo = core_plugin_manager::instance()->get_plugin_info('local_ohflib');
if ( is_null( $plugininfo ) ) {
	print_error( "The required plugin 'local_ohflib' is not installed. Contact your system administrator." ); // execution dies here
} else {
	require_once $plugininfo->rootdir . "/ohflib.php";
	$PIMObj = new ohflib_PluginInfo_Manager( __FILE__ );
}
//**********************************************************************************************

global $CFG, $PAGE;

$plugin_name = 'local_amp';
//$PAGE->set_url('/admin/settings.php', array( 'section' => $plugin_name ) );

// TODO: Add mechanism to select an "Apply Now" graphic button (based on language) - kaw
// TODO: Create new @@ApplyNowButton:[name]@@ VarTag driven by AMP setting
// TODO: Create a new top-level module to manage "Apply Now" graphic buttons - kaw

if ( ! $PAGE->headerprinted ) {
	// These CSS/JS declarations are required when the AMP settings page is being
	// rendered, but this file is included by Moodle for other purposes beyond
	// rendering the settings. In those cases, these declarations are not only
	// unnecessary, but since Moodle has already output a page header, these
	// declarations cause a crash. Therefore these declarations must only be
	// allowed when a page header has NOT been output.
	$PAGE->requires->css( new moodle_url( '/local/ohflib/css/jquery-ui.css' ) );
	$PAGE->requires->js( '/local/ohflib/js/jquery-1.10.2.js', TRUE );
	$PAGE->requires->js( '/local/ohflib/js/jquery-ui.js', TRUE );
	$PAGE->requires->js( '/local/ohflib/js/default-value-dialog.js', TRUE );
}

$settings = new admin_settingpage( $plugin_name, get_string( 'pluginname', $plugin_name ) );
$ADMIN->add('localplugins', $settings);

$label = 'Application Package Selection';
$desc = 'AMP implements the process of a user making application to assume a role in an organization. '
	. 'For example, a user wants to apply to become a student of a school or university. The application process '
	. 'may or may not require email address validation (a common need), but if so, then an email is sent (to the '
	. 'applicant) containing a link which validates the address and opens the applicant-selected application. Once '
	. 'the application is submitted, a staff member evaluates it and accepts or rejects it. If accepted, AMP '
	. 'creates the Moodle user account and sends an email with acceptance confirmation and the login credentials.'

	. '<br><br>AMP depends on the use of one or more application-package plugins being installed.'

	. '<br><br>An application-package consists of the application form itself, any related reference forms and one or '
	. 'more related text pages (instructions, guides, etc). An application-package is defined when its '
	. 'implementing plugin is installed. AMP automatically recognizes application-definition plugins and lists '
	. 'them here so that one can be selected.';
$settings->add( new admin_setting_heading( 'amp_intro_heading', $label, $desc ) );
$choices = amp_buildApplicationPkgOptionList( __FILE__, __LINE__ );
if ( count( $choices ) == 0 ) {
	// There are no currently loaded application-definition plugins (and the Moodle admin settings mechanism
	// simply doesn't display any picklist or multiselect control having no options), so clear the org selection
	// and display a message block to indicate this condition
	set_config( 'amp_pkgselection', '' );
	$label = '';
	$desc = 'There are no installed application-package definition plugins. Install one or more, then refresh this page';
	$settings->add( new admin_setting_heading( 'amp_noorgs_heading', $label, $desc ) );
} else {
	// At least one application-definition plugin is defined
	$label = 'Selected application package';
	$desc = 'The applications defined in the package selected here will be offered as choices to applicants.';
	$pkg_selection = isset( $CFG->amp_pkgselection ) ? $CFG->amp_pkgselection : FALSE ;
	if ( $pkg_selection === FALSE ) {
		// There is no currently-selected application package, so render only the selection setting control
		$settings->add( new admin_setting_configselect( 'amp_pkgselection', $label, $desc, 'xxx', $choices ) );
	} else {
		// There is a selected package, so render the selection setting control (with a link to the
		// settings page of the application package) and the all the other settings
		$pkgObj = amp_newApplicationPkgObj( $pkg_selection );
		$url = $pkgObj->makeSettingsPageURL();
		$desc .= "<br /><a href='$url' target='_blank'>View package settings</a> in a new browser tab.";
		$settings->add( new admin_setting_configselect( 'amp_pkgselection', $label, $desc, 'xxx', $choices ) );

		// Render the Validation Control heading (the $EVRMgr object will not be used, but instantiating it
		// creates a number of empty VarTags whose definitions should be included in the display below)
		$EVRMgr = new amp_EVR_Manager( FALSE ); // This will create EVR-related VarTags with blank values (and ignoring the 'invalid application type error)
		$vtmgrObj = new ohflib_VarTagManager();
		$label = "Validation Control";
		$desc = "The following settings control application processing and define the templates for emails sent during email validation.";
		$desc .= $vtmgrObj->renderDisplay( 'datetime|application|package' );
		$settings->add( new admin_setting_heading( 'amp_email_controls', $label, $desc ) );

		// Validation email settings
		$label = 'Validation Email';
		$desc = 'The following settings control email validation and define the properties of the validation email message.';

		$label = 'Enable address validation?';
		$desc = 'Checkmark indicates that the email address validation process will be ENABLED.';
		$settings->add( new admin_setting_configcheckbox('amp_validateemail', $label, $desc, 0 ) );

		$label = 'Validation page introduction template';
		$control_id = 'amp_valpageintrotemplate';
		$desc = "Intro page template used when email validation is ENABLED and 1 or more applications are defined.";
		$desc = ohflib_renderSettingControlDesc( $plugin_name, $label, $control_id, $desc );
		$settings->add( new admin_setting_confightmleditor( $control_id, $label, $desc, NULL, PARAM_RAW, 40, 12 ) );

		$label = 'Validation email FROM address';
		$desc = 'The address from which validation emails should appear to come.';
		$settings->add( new admin_setting_configtext('amp_validationemailaddr', $label, $desc, '', PARAM_RAW, 35 ) );

		$label = 'Validation email BCC addresses';
		$desc = 'The addresses (one per line) to which validation emails should be blind copied. This feature is typically used for testing or monitoring.';
		$settings->add( new admin_setting_configtextarea('amp_validationemailbccaddr', $label, $desc, '', PARAM_RAW, 20, 5 ) );

		$label = 'Validation email Subject';
		$desc = 'The subject line for validation emails.';
		$default = get_string( 'amp_validationemailsubj_default', $plugin_name );
		$settings->add( new admin_setting_configtext('amp_validationemailsubj', $label, $desc, $default, PARAM_RAW, 80 ) );

		$label = 'Validation email body template';
		$control_id = 'amp_validationmsgtemplate';
		$desc = "<strong>Plain-text</strong> template for the body of the validation email message for a single defined application";
		$desc = ohflib_renderSettingControlDesc( $plugin_name, $label, $control_id, $desc );
		//$settings->add( new admin_setting_confightmleditor( $control_id, $label, $desc, NULL, PARAM_RAW, 40, 6 ) );
		$settings->add( new admin_setting_configtextarea( $control_id, $label, $desc, NULL, PARAM_RAW, 100, 14 ) );

		$label = 'Validation response page content';
		$control_id = 'amp_validationresponsepage';
		$desc = "The page displayed to users who submit an email validation form.";
		$desc = ohflib_renderSettingControlDesc( $plugin_name, $label, $control_id, $desc );
		$settings->add( new admin_setting_confightmleditor( $control_id, $label, $desc, NULL, PARAM_RAW, 40, 6 ) );

		$label = 'Management email address';
		$desc = 'The address to which application-related management notifications should be sent.';
		$settings->add( new admin_setting_configtext('amp_mgmtemailaddr', $label, $desc, '', PARAM_RAW, 35 ) );
	}
}
