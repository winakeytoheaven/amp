<?php
/**
 * The AMP plugin's Moodle event observer(s).
 *
 * NOTE: Changes here will not be seen in site activity until all Moodle caches are purged.
 *
 * @package   localamp
 * @copyright 2016 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$observers = array(

	array(
		'eventname'		=> '\core\event\course_viewed',
		'callback'		=> 'amp_event_listener::course_viewed',
		'includefile'	=> '/local/amp/classes/class.EventListener.php'
	)
);