<?php

/**
 * This file contains any classes and functions required during plugin installation
 * and/or upgrade.
 *
 * @package   localamp
 * @copyright 2017 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


//**********************************************************************************************
/**
 * Creates any settings (with default values) which are required by this plugin. Doing this
 * during plugin installation or update relieves the rest of the plugin codebase from having
 * to worry about undefined setting variables.
 *
 * @return	void
 */
function amp_createSettings() {

	// Get this plugin's defined settings
	$settingMgr = new ohflib_Settings_Mgr( 'local_amp' );
	$new_settings = $settingMgr->createSettings();

	return;

} // end of function amp_createSettings()


//**********************************************************************************************
/**
 * Deletes all the settings associated with this plugin from Moodle's option tables.
 *
 * @return	void
 */
function amp_deleteSettings() {

	// Get this plugin's defined settings
	$settingMgr = new ohflib_Settings_Mgr( 'local_amp' );
	$settingMgr->deleteSettings();

	return;

} // end of function amp_deleteSettings()