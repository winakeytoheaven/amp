<?php

/**
 * Invoked directly by Moodle during AMP upgrades.
 *
 * @package   localamp
 * @copyright 2016 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function xmldb_local_amp_upgrade( $oldversion = 0 ) {

	global $CFG;

	// Load any required modules from OHFLIB
	$ohflib_root = $CFG->dirroot . '/local/ohflib';
	require_once( "$ohflib_root/classes/class.Settings_Mgr.php" );

	// Load this plugin's modules required for installation or upgrading
	$plugin_root = $CFG->dirroot . '/local/amp';
	require_once( "$plugin_root/db/upgradelib.php" );

	// Create any new plugin settings with default values
	amp_createSettings();

	return TRUE;

} // end of function xmldb_local_amp_upgrade()
