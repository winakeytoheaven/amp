<?php

/**
 * Creates array-based elements which define an API for the AMP plugin. The values here exist
 * mostly as a proof-of-concept and don't offer anything more than a 'hello world' response
 * to an external web-service request.
 *
 * @package   localamp
 * @copyright 2016 OneHundredFold (http://100fold.it)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// We defined the web service functions to install
$functions = array(
	'local_amp_hello_world' => array(
		'classname' => 'local_amp_external',
		'methodname' => 'hello_world',
		'classpath' => 'local/mmp/externallib.php',
		'description' => 'Return Hello World FIRSTNAME. Can change the text (Hello World) sending a new text as parameter',
		'type' => 'read'
	)
);

// We define the services to install as pre-build services. A pre-build service is not editable by administrator.
$services = array(
	'Application Management Plugin' => array(
		'functions' => array(
			'local_amp_hello_world'
		),
		'restrictedusers' => 0,
		'enabled' => 1
	)
);
