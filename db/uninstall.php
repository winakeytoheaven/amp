<?php

/**
 * Contains a single function which is invoked during Moodle's plugin-installation process.
 *
 * @package   localamp
 * @copyright 2017 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

//**********************************************************************************************
/**
 * This function performs functionality required during TEST plugin installation
 *
 * @return	void
 */
function xmldb_local_amp_uninstall() {

	global $CFG;

	// Load any required modules from OHFLIB
	$ohflib_root = $CFG->dirroot . '/local/ohflib';
	require_once( "$ohflib_root/classes/class.Settings_Mgr.php" );

	// Load this plugin's modules required for installation or upgrading
	$plugin_root = $CFG->dirroot . '/local/amp';
	require_once( "$plugin_root/db/upgradelib.php" );

	// Create plugin settings with default values
	amp_deleteSettings();

	return;

} // end of function xmldb_local_amp_uninstall()

