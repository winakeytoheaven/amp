<?php

/**
 * This file renders only the shell of an MMP page, and was apparently never developed further.
 *
 * @package   localamp
 * @copyright 2016 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

//**********************************************************************************************
// OHFLIB initialization - For details see <document_root>/local/ohflib/dev/initialize.php.
//**********************************************************************************************
require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';
$plugininfo = core_plugin_manager::instance()->get_plugin_info('local_ohflib');
if ( is_null( $plugininfo ) ) {
	print_error( "The required plugin 'local_ohflib' is not installed. Contact your system administrator." ); // execution dies here
} else {
	require_once $plugininfo->rootdir . "/ohflib.php";
	$PIMObj = new ohflib_PluginInfo_Manager( __FILE__ );
}
//**********************************************************************************************

require_once($CFG->dirroot . '/local/amp/lib.php');

require_login();

// TODO: Add sesskey check to edit
//$edit   = optional_param('edit', null, PARAM_BOOL);    // Turn editing on and off

$header = get_string('pluginname', 'local_amp');


// Start setting up the page
$params = array();
$PAGE->set_context($context);
$PAGE->set_url('/local/amp/index.php', $params);
$PAGE->set_pagelayout('mydashboard');
$PAGE->set_pagetype('my-index');
$PAGE->blocks->add_region('content');
$PAGE->set_subpage($currentpage->id);
$PAGE->set_title($header);
$PAGE->set_heading($header);


echo $OUTPUT->header();

echo $OUTPUT->blocks_for_region('content');

echo $OUTPUT->footer();
