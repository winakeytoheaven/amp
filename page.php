<?php

/**
 * Renders the specified "related page" of a specified application. Both the application and
 * the related page must be defined in a OHF-family plugin of usage "application_pkg" as
 * determined by the function [plugin-name]_getPluginInfo() in its ohflib.php.
 *
 * @package   localbtsccm
 * @copyright 2016 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

global $CFG, $PAGE, $OUTPUT;
require_once $_SERVER['DOCUMENT_ROOT'] . '/config.php';

//**********************************************************************************************
// OHFLIB initialization - For details see <document_root>/local/ohflib/dev/initialize.php.
//
$plugininfo = core_plugin_manager::instance()->get_plugin_info('local_ohflib');
if ( is_null( $plugininfo ) ) {
	print_error( "The required plugin 'local_ohflib' is not installed. Contact your system administrator." ); // execution dies here
} else {
	require_once $plugininfo->rootdir . "/ohflib.php";
	$PIMObj = new ohflib_PluginInfo_Manager( __FILE__ );
}
//**********************************************************************************************

$plugin_name = 'local_amp';

// Get the action and validation request ID from the URL
$app_type = ohflib_getURLParam( 'apptype', '' );
$page_id = ohflib_getURLParam( 'pid', '' );

$appObj = amp_newApplicationPkgObj();

// Set up the page infrastructure
$PAGE->set_context( context_system::instance() );
$PAGE->set_url( '/local/amp/page.php', array( 'apptype' => $app_type, 'pid' => $page_id ) );
$PAGE->set_pagelayout( 'frontpage' );
$PAGE->blocks->add_region( 'content' );
$header = $appObj->getPageTitle( $app_type, $page_id );
$PAGE->set_title( $header );
$PAGE->set_heading( $header );

// Every action past this point renders a page, so set up the page content
echo $OUTPUT->header();
echo $OUTPUT->blocks_for_region( 'content' );

echo $appObj->renderPage( $app_type, $page_id );

// Finish up the page output
echo $OUTPUT->footer();
