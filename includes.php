<?php
/**
 * This module should only be invoked by the OHFLIB initialization process.
 *
 * Any new library or class module created for this plugin must be included here.
 *
 * @package   localamp
 * @copyright 2016 OneHundredFold (http://100fold.org)
 * @author    Ken Wienecke
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

global $CFG;

$path = $CFG->dirroot . "/local/amp";

// Plugin-root libraries (do NOT include lib.php because Moodle will do that and because
// the OHFLIB initialization will happen there if required)
require_once $path . '/locallib.php';

// classes
$classes = "$path/classes";
require_once "$classes/class.EVRForm.php";
require_once "$classes/class.EVRManager.php";
